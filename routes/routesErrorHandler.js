const isProduction = process.env.NODE_ENV === 'production';

module.exports = app => {
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    if (!isProduction) {
        app.use(function(err, req, res) {
            console.log(err.stack);

            res.status(err.status || 500);

            res.json({'errors': {
                    message: err.message,
                    error: err
                }});
        });
    }

    app.use((err, req, res) => {
        res.status(err.status || 500);
        res.json({'errors': {
                message: err.message,
                error: {}
            }});
    });
};