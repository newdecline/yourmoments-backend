const router = require('express').Router();
const accessTokenCheck = require('../middlewares/accessTokenCheck');

// /api
router.use(accessTokenCheck);
router.use('/api', require('./api'));

module.exports = router;