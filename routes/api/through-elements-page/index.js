const router = require('express').Router();
const throughElementsPageController = require('../../../controllers/throughElementsPageController');
const bodyParser  = require("body-parser");

const jsonParser = bodyParser.json();

// /api/through-elements-page
router.get('/', throughElementsPageController.getThroughElements);
router.put('/', jsonParser, throughElementsPageController.updateThroughElements);

module.exports = router;