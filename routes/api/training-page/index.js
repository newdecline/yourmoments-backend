const router = require('express').Router();
const multer = require('multer');
const bodyParser  = require("body-parser");
const trainingPageController = require('../../../controllers/trainingPageController');

const upload = multer({}).any();
const jsonParser = bodyParser.json();

router.get('/', trainingPageController.getTrainingPage);

router.put('/', upload, trainingPageController.updateTrainingPage);

router.get('/lessons', trainingPageController.getLessons);

router.post('/lessons', upload, trainingPageController.createLessons);

router.put('/lessons', upload, trainingPageController.updateLesson);

router.put('/lessons/hide', upload, trainingPageController.updateHideLesson);

router.put('/lessons/move', jsonParser, trainingPageController.moveLesson);

router.delete('/lessons', upload, trainingPageController.deleteLessons);

module.exports = router;