const router = require('express').Router();
const PhotoSeriesGalley = require('../../../models/PhotoSeriesGalley');

router.delete('/:galleryId/:imageId', (req, res, next) => {
  PhotoSeriesGalley.deleteImage(req)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => {return next(err)});
});

module.exports = router;