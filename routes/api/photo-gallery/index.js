const router = require('express').Router();

router.use('/', require('./getImages'));
router.use('/', require('./addImages'));
router.use('/', require('./deleteImage'));

module.exports = router;