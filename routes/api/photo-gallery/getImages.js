const router = require('express').Router();
const PhotoSeriesGalley = require('../../../models/PhotoSeriesGalley');

router.get('/:id', (req, res, next) => {
  PhotoSeriesGalley.getImages(req)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => {return next(err)});
});

module.exports = router;