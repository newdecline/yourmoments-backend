const router = require('express').Router();
const PhotoSeriesGalley = require('../../../models/PhotoSeriesGalley');
const multer = require('multer');
const {SAVE_IMAGE_PATH} = require('../../../constants');

//Какие типы картинок разрешены
const allowedMimeTypes = ['image/jpeg'];

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `.${SAVE_IMAGE_PATH}`);
  },
  filename: function (req, file, cb) {
    const fileName = `gallery-image-${new Date().getTime().toString()}.jpg`;
    PhotoSeriesGalley.addImages(req, fileName);
    cb(null, fileName);
  }
});

const fileFilter = (req, file, cb) => {
  if (allowedMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    return cb({message: 'Формат файла не верный'}, false);
  }
};

const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 15
  },
  fileFilter
}).any();

router.post('/:id', (req, res, next) => {
  upload(req, res, async function (err) {
    if (err) {return next(err)}

    try {
      const gallery = await PhotoSeriesGalley.getImages(req);
      res.status(200).json(gallery);
    } catch (err) {
      return next(err)
    }
  })
});

module.exports = router;