const router = require('express').Router();
const multer = require('multer');
const PhotoSeries = require('../../../models/PhotoSeries');

const upload = multer({}).any();

router.post('/', upload, (req, res, next) => {
  const {name, category} = req.body;

  PhotoSeries.createPhotoSeries(name, category)
    .then(data => {
      res.status(201).json(data);
    })
    .catch(err => next(err));
});

module.exports = router;