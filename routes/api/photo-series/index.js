const router = require('express').Router();

router.use('/', require('./getPhotoSeries'));
router.use('/', require('./createSeries'));
router.use('/', require('./updateSeries'));
router.use('/', require('./deleteSeries'));

module.exports = router;