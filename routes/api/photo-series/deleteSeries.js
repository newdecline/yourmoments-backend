const router = require('express').Router();
const multer = require('multer');
const PhotoSeries = require('../../../models/PhotoSeries');

const upload = multer({}).any();

router.delete('/', upload, async (req, res, next) => {
  const {slug} = req.body;

  PhotoSeries.deletePhotoSeries({slug}).then(data => {
    return res.status(200).json(data);
  }).catch(err => {
    next(err);
  });
});

module.exports = router;