const router = require('express').Router();
const fs = require('fs');
const PhotoSeries = require('../../../models/PhotoSeries');
const multer = require('multer');
const {SAVE_IMAGE_PATH} = require('../../../constants');
const isDirectoryExists = require('../../../helpers/isDirectoryExists');

//Создать папку для картинок под фотосерии если ее нет
fs.mkdirSync(`.${SAVE_IMAGE_PATH}`, {recursive: true}, (err) => {
  if (err) throw err;
});

isDirectoryExists(`.${SAVE_IMAGE_PATH}`);

//Какие типы картинок разрешены
const allowedMimeTypes = ['image/jpeg'];

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `.${SAVE_IMAGE_PATH}`);
  },
  filename: function (req, file, cb) {
    const fileName = `${file.fieldname}-${new Date().getTime().toString()}.jpg`;

    cb(null, fileName);
  }
});

const fileFilter = (req, file, cb) => {
  if (allowedMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    return cb({message: 'Формат файла не верный'}, false);
  }
};

const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 15
  },
  fileFilter
}).any();

router.put('/', (req, res, next) => {
  upload(req, res, async function (err) {
    if (err) {
      return next(err)
    }

    PhotoSeries.updatePhotoSeries(req)
      .then(data => {
        return res.status(200).json(data);
      })
      .catch(err => next(err));
  })
});

module.exports = router;