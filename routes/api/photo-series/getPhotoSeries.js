const router = require('express').Router();
const PhotoSeries = require('../../../models/PhotoSeries');

router.get('/', (req, res, next) => {
  PhotoSeries.getPhotoSeries(req.query)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => next(err));
});

module.exports = router;