const router = require('express').Router();
const bodyParser  = require("body-parser");
const {loginController} = require('../../../controllers/loginController');
const {logoutController} = require('../../../controllers/logoutController');

router.use(bodyParser.json());

router.post('/login', loginController);
router.post('/logout', logoutController);

module.exports = router;