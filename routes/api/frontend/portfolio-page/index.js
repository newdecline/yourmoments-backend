const router = require('express').Router();
const PhotoSeries = require('../../../../models/PhotoSeries');
const PhotoSeriesGalley = require('../../../../models/PhotoSeriesGalley');

// /api/frontend/portfolio-page
router.get('/', async (req, res, next) => {
  PhotoSeries.getPhotoSeries()
    .then(data => {
      return res.status(200).json({
        videoUrls: data.filter(series => !!series.urlVideo).map(series => ({
          videoUrl: series.urlVideo,
          name: series.name
        }))
      })
    })
    .catch(err => {
      console.log(err);
    });
});

router.get('/:category', async (req, res, next) => {
  const {category} = req.params;

  try {
    const photoSeries = await PhotoSeries.getPhotoSeries({category});
    const series = photoSeries.filter(series => series.originalImage).map(series => ({
      slug: series.slug,
      croppedCardImage: series.croppedCardImage,
      name: series.name
    }));

    return res.status(200).json({
      series
    })
  } catch (err) {
    console.log(err);
  }
});

router.get('/:category/:slug', async (req, res, next) => {
  const {slug} = req.params;

  try {
    const series = await PhotoSeries.getPhotoSeries({slug});

    if (series.length === 0 || !series[0].originalImage) {
      return res.status(404).json({message: 'Фотосерии не существует'})
    }

    const gallerySeries = await PhotoSeriesGalley.findOne({_id: series[0].galleryImages});

    return res.status(200).json({
      series: series[0],
      gallerySeries: gallerySeries.images
    });

  } catch (err) {
    console.log(err);
  }
});

module.exports = router;