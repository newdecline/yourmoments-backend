const router = require('express').Router();
const ThroughElements = require('../../../../models/ThroughElements');

// /api/frontend/through-elements
router.get('/', async (req, res, next) => {
  try {
    const throughElements = await ThroughElements.findOne({});

    return res.status(200).json(throughElements)
  } catch (err) {
    return next(err);
  }
});

module.exports = router;