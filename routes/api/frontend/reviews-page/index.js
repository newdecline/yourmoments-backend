const Reviews = require('../../../../models/Reviews');
const router = require('express').Router();

// /api/frontend/reviews-page
router.get('/', async (req, res, next) => {
  try {
    const reviews = await Reviews.find({}).sort([['position']]).exec() || [];

    return res.status(200).json(reviews);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;