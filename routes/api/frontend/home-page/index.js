const router = require('express').Router();
const HomePageSlider = require('../../../../models/HomePageSlider');

// /api/frontend/home-page
router.get('/', async (req, res, next) => {
  try {
    const slider = await HomePageSlider.find({}).exec();
    let slides = [];

    if (slider.length) {
      slides = slider[0].slides.sort((a, b) => a.position - b.position);
    }

    return res.status(200).json({
      banner: slides
    });
  } catch (err) {
    return next(err);
  }
});

module.exports = router;