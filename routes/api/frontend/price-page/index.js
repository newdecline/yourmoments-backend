const router = require('express').Router();
const Tariff = require('../../../../models/Tariff');

// /api/frontend/price-page
router.get('/', async (req, res, next) => {
  Tariff.getTariffs(req)
    .then(data => {
      return res.status(200).json(data)
    })
    .catch(err => {
      return next(err)
    });
});

module.exports = router;