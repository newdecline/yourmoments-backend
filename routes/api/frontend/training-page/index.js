const router = require('express').Router();
const TrainingPage = require('../../../../models/TrainingPage');
const Lesson = require('../../../../models/Lesson');

// /api/frontend/training-page
router.get('/', async (req, res, next) => {
  try {
    const pageData = await TrainingPage.findOne({});
    const lessons = await Lesson.find({hide: true}).sort([['position']]).exec();

    return res.status(200).json({
      pageData,
      lessons
    })
  } catch (err) {
    return next(err);
  }
});

module.exports = router;