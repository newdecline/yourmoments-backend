const router = require('express').Router();

router.use('/home-page', require('./home-page'));
router.use('/portfolio-page', require('./portfolio-page'));
router.use('/price-page', require('./price-page'));
router.use('/training-page', require('./training-page'));
router.use('/reviews-page', require('./reviews-page'));
router.use('/contacts-page', require('./contacts-page'));
router.use('/through-elements', require('./through-elements'));

module.exports = router;