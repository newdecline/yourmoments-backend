const router = require('express').Router();

// /api/price
router.use('/tariff', require('./tariff'));

module.exports = router;