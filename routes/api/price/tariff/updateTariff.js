const router = require('express').Router();
const multer = require("multer");
const Tariff = require('../../../../models/Tariff');

const upload = multer({}).any();

// /api/price/tariff
router.put('/', upload, (req, res, next) => {
  Tariff.updateTariff(req)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => next(err));
});

module.exports = router;