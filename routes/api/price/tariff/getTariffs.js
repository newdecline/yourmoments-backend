const router = require('express').Router();
const Tariff = require('../../../../models/Tariff');

// /api/price/tariff
router.get('/', (req, res, next) => {
  Tariff.getTariffs(req)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => next(err));
});

module.exports = router;