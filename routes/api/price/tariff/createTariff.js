const router = require('express').Router();
const multer = require('multer');
const Tariff = require('../../../../models/Tariff');

const upload = multer({}).any();

// /api/price/tariff
router.post('/', upload, (req, res, next) => {
  Tariff.createTariff(req)
    .then(data => {
      res.status(201).json(data);
    })
    .catch(err => next(err));
});

module.exports = router;