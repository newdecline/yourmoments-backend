const router = require('express').Router();
const bodyParser  = require("body-parser");
const Tariff = require('../../../../models/Tariff');

const jsonParser = bodyParser.json();

// /api/price/tariff
router.put('/move', jsonParser, (req, res, next) => {
  Tariff.moveTariff(req)
    .then(data => {
      res.status(200).json(data);
    })
    .catch(err => next(err));
});

module.exports = router;