const router = require('express').Router();

// /api/price/tariff
router.use('/', require('./createTariff'));
router.use('/', require('./getTariffs'));
router.use('/', require('./updateTariff'));
router.use('/', require('./deleteTariff'));
router.use('/', require('./moveTariff'));

module.exports = router;