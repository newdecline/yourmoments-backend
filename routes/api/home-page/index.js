const router = require('express').Router();
const homePageSliderController = require("../../../controllers/homePageSliderController");
const bodyParser  = require("body-parser");

const jsonParser = bodyParser.json();

// /api/homepage
router.get('/banner', homePageSliderController.getSlides);
router.put('/banner', homePageSliderController.updateSlide);
router.put('/banner/move-slide', jsonParser, homePageSliderController.moveSlide);
router.delete('/banner', homePageSliderController.deleteSlide);

module.exports = router;