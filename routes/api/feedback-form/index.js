const router = require('express').Router();
const bodyParser = require('body-parser');
const nodemailer = require("nodemailer");
const { stringify } = require('querystring');
const fetch = require('node-fetch');

const secretKey = process.env.GOOGLE_SECRET_KEY;

router.use(bodyParser.urlencoded({extended: true}));
router.use(bodyParser.json());

router.post('/', async (req, res, next) => {
    console.log('send form');
    const {name, socialNetwork, date, captcha} = req.body;

    const query = stringify({
        secret: secretKey,
        response: captcha,
        remoteip: req.connection.remoteAddress
    });
    const verifyURL = `https://google.com/recaptcha/api/siteverify?${query}`;

    const body = await fetch(verifyURL).then(res => res.json());

    if (body.success !== undefined && !body.success) {
        return res.status(400).send({message: 'Ошибка проверки каптчи'}).end();
    }

    let message = {
        from: process.env.MAIL_USER,
        to: process.env.MAIL_USER,
        subject: `Новая заявка с сайта`,
        html: `
    <style>
        p {
            font-size: 20px;
            font-weight: bold;
        }
        th {
            font-size: 16px;
            background: #496791;
            color: #fff;
        }
        td {
            font-size: 16px;
            background: #f5e8d0;
        }
        td, th {
            font-size: 16px;
            padding: 5px 10px;
            vertical-align: top;
        }
    </style>
    <p>Информация о клиенте</p>
    <table>
        <tr>
            <td>Имя</td>
            <td>${name}</td>
        </tr>
        <tr>
            <td>Способ связи</td>
            <td>${socialNetwork}</td>
        </tr>
        <tr>
            <td>Желаемая дата бронирования</td>
            <td>${date}</td>
        </tr>
    </table>`,
    };

    const transporter = nodemailer.createTransport(
        {
            host: 'smtp.mail.ru',
            port: 465,
            secure: true,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASSWORD
            }
        }
    );

    transporter.sendMail(message, (err, info) => {
        if (err) {
            return next(err);
        }

        res.status(200).send({
            message: 'Form Submitted Successfully',
            captcha: true
        }).end();
    });

});

module.exports = router;