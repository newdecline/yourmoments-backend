const router = require('express').Router();
const allowIfLoggedIn = require('./../../middlewares/allowIfLoggedIn');

router.use('/auth', require('./auth'));

router.use('/feedback-form', require('./feedback-form'));
router.use('/frontend', require('./frontend'));

router.use('/photo-series', allowIfLoggedIn, require('./photo-series'));
router.use('/photo-gallery', allowIfLoggedIn, require('./photo-gallery'));
router.use('/price', allowIfLoggedIn, require('./price'));
router.use('/training-page', allowIfLoggedIn, require('./training-page'));
router.use('/reviews-page', allowIfLoggedIn, require('./reviews-page'));
router.use('/home-page', allowIfLoggedIn, require('./home-page'));
router.use('/through-elements-page', allowIfLoggedIn, require('./through-elements-page'));

module.exports = router;