const router = require('express').Router();
const multer = require('multer');
const bodyParser  = require("body-parser");
const reviewsPageController = require("../../../controllers/reviewsPageController");

const upload = multer({}).any();
const jsonParser = bodyParser.json();

router.get('/', reviewsPageController.getReviews);
router.post('/', jsonParser, reviewsPageController.createReviews);
router.put('/', jsonParser, reviewsPageController.updateReviews);
router.put('/image', reviewsPageController.updateImageReviews);
router.put('/move', jsonParser, reviewsPageController.moveReviews);
router.delete('/', jsonParser, reviewsPageController.deleteReviews);

module.exports = router;