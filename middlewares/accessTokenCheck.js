const jwt = require('jsonwebtoken');
const User = require('./../models/User');
const AccessToken = require('./../models/AccessToken');

const accessTokenCheck = async (req, res, next) => {
  if (req.headers["x-access-token"]) {
    const accessToken = req.headers["x-access-token"];

    await jwt.verify(accessToken, process.env.JWT_SECRET, async (err, data) => {
      if (err) return res.status(401).json({
        message: 'Неправильно указан токен JWT, войдите, чтобы получить новый'
      });

      const hasAccessToken = await AccessToken.findOne({value: accessToken});
      if (!hasAccessToken) {
        return res.status(401).json({message: 'Токен JWT, недействителен, получите новый'});
      }

      const {userId, exp} = data;

      if (exp < Date.now().valueOf() / 1000) {
        return res.status(401).json({message: 'Срок действия токена JWT истек, войдите, получите новый'});
      }

      res.locals.loggedInUser = await User.findById(userId);
      next();
    });
  } else {
    next();
  }
};

module.exports = accessTokenCheck;