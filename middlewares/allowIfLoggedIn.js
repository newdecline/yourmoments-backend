const allowIfLoggedIn = async (req, res, next) => {
  const user = res.locals.loggedInUser;

  if (!user) {
    return res.status(401).json({
      message: "Необходимо войти"
    });
  }

  req.user = user;
  next();
};

module.exports = allowIfLoggedIn;