const {Image} = require('../models/Image');
const fs = require('fs');
const multer = require('multer');
const {cropImage} = require('../helpers/cropImage');
const {initialHomePageSliderModel} = require("../helpers/initialHomePageSliderModel");
const HomePageSlider = require('../models/HomePageSlider');

const {v4} = require('uuid');
const {HOMEPAGE_BANNER_IMAGE_PATH} = require('../constants');
const isDirectoryExists = require('../helpers/isDirectoryExists');

//Создать папку для картинок под фотосерии если ее нет
fs.mkdirSync(`.${HOMEPAGE_BANNER_IMAGE_PATH}`, {recursive: true}, (err) => {
  if (err) throw err;
});

isDirectoryExists(`.${HOMEPAGE_BANNER_IMAGE_PATH}`);

//Какие типы картинок разрешены
const allowedMimeTypes = ['image/jpeg'];

//Допустимый максимальный размер файла
const allowedMaxSizeFile = 1024 * 1024 * 15;

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `.${HOMEPAGE_BANNER_IMAGE_PATH}`);
  },
  filename: function (req, file, cb) {
    const fileName = `${file.fieldname}-${new Date().getTime().toString()}.jpg`;

    cb(null, fileName);
  }
});

const fileFilter = (req, file, cb) => {
  if (allowedMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    return cb({message: 'Формат файла не верный'}, false);
  }
};

const upload = multer({
  storage,
  limits: {
    fileSize: allowedMaxSizeFile
  },
  fileFilter
}).any();

exports.getSlides = async (req, res, next) => {
  try {
    await initialHomePageSliderModel();

    const slider = await HomePageSlider.find(req.query).exec();
    const slides = slider[0].slides.sort((a, b) => a.position - b.position);

    return res.status(200).json(slides);
  } catch (err) {
    return next(err);
  }
};

exports.updateSlide = async (req, res, next) => {
  upload(req, res, async function (err) {
    if (err) {
      return next(err)
    }

    const slider = await HomePageSlider.find({});
    const sliderId = slider[0]._id;

    const originalImageFile = req.files[0];
    const {originalImageId, croppedImage} = req.body;

    let update = {
      slideId: v4(),
      position: -1
    };

    //Полный путь где лежит оригинал картинки
    const originalImagePath = (originalImageFile && `./${originalImageFile.path}`) || `.${req.body.originalImage}`;

    //Если нужно удалить оригинальную картинку фотосерии
    if (originalImageFile && originalImageId) {
      try {
        await Image.deleteImage(originalImageId);
      } catch (e) {
        console.log(e);
      }
    }

    //Если нужно созать новую оригинальную картинку
    if (originalImageFile && !req.body.originalImage) {
      try {
        const newOriginalImage = await Image.saveImage(`${HOMEPAGE_BANNER_IMAGE_PATH}/${originalImageFile.filename}`, originalImageFile.filename);
        update = {
          ...update,
          originalImage: newOriginalImage
        };
      } catch (e) {
        console.log(e);
      }
    }

    //Если нужно обновить или создать обрезанную картинку
    if (croppedImage.cropperData) {
      const imageName = `croppedImage-${new Date().getTime().toString()}.jpg`;
      const croppedImagePath = `${HOMEPAGE_BANNER_IMAGE_PATH}/${imageName}`;
      const {width, height, x, y} = croppedImage.cropperData;

      if (croppedImage.imageId) {
        try {
          await Image.deleteImage(croppedImage.imageId);
        } catch (e) {
          console.log(e);
        }
      }

      try {
        await cropImage(
          originalImagePath,
          width,
          height,
          x,
          y,
          `.${croppedImagePath}`);
      } catch (e) {
        console.error(e);
      }


      try {
        const newCroppedImage = await Image.saveImage(croppedImagePath, imageName);
        update = {
          ...update,
          croppedImage: newCroppedImage
        };
      } catch (e) {
        console.log(e);
      }
    }

    try {
      await HomePageSlider.findOneAndUpdate(
        {_id: sliderId},
        {"$push": {slides: update}});

      const slider = await HomePageSlider.find({});
      const slides = slider[0].slides.sort((a, b) => a.position - b.position);

      return res.status(200).json(slides);
    } catch (err) {
      return next(err);
    }
  });
};

exports.moveSlide = async (req, res, next) => {
  try {
    const _slider = await HomePageSlider.find({}).limit(1);
    const sliderId = _slider[0]._id;

    const test = req.body.map(async slide => {
      const $elemMatch = {slideId: slide.slideId};

      return await HomePageSlider.findOneAndUpdate({_id: sliderId, slides: {$elemMatch}},
        {
          $set: {
            'slides.$.position': slide.position,
          }
        });
    });

    Promise.all(test).then(async data => {
      const slider = await HomePageSlider.find({});
      const slides = slider[0].slides.sort((a, b) => a.position - b.position);

      return res.status(200).json(slides);
    });
  } catch (err) {
    return next(err);
  }
};

exports.deleteSlide = async (req, res, next) => {
  try {
    const {
      slideId,
      originalImage,
      croppedImage
    } = req.query;

    const _slider = await HomePageSlider.find({});
    const sliderId = _slider[0]._id;

    const idImages = [
      originalImage,
      croppedImage,
    ].filter(item => item);

    //Удалить картинку баннера
    for (let i = 0; i < idImages.length; i++) {
      try {
        await Image.deleteImage(idImages[i]);
      } catch (e) {
        console.error(e);
      }
    }

    await HomePageSlider.findOneAndUpdate(
      {_id: sliderId},
      {"$pull": {slides: {slideId}}});

    const slider = await HomePageSlider.find({});
    const slides = slider[0].slides.sort((a, b) => a.position - b.position);

    return res.status(200).json(slides);
  } catch (err) {
    return next(err);
  }
};