const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User  = require('../models/User');
const AccessToken = require('./../models/AccessToken');

const validatePassword = async (plainPassword, hashedPassword) => {
  return await bcrypt.compare(plainPassword, hashedPassword);
};

exports.loginController = async (req, res, next) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user) return res.status(401).json({message: 'Не верный логин или пароль'});

  const validPassword = await validatePassword(password, user.password);

  if (!validPassword) return res.status(401).json({message: 'Не верный логин или пароль'});

  const accessUserTokens = await AccessToken.find({userId: user._id});

  if (accessUserTokens.length === +process.env.LIMIT_ACCESS_TOKENS) {
    await AccessToken.remove({_id: accessUserTokens[0]._id});
  }

  const accessToken = jwt.sign({userId: user._id}, process.env.JWT_SECRET, {
    expiresIn: "1d"
  });

  const newAccessToken = new AccessToken({
    userId: user._id,
    value: accessToken
  });

  newAccessToken.save((err, value) => {
    if (err) {
      return next(err)
    }

    user.accessToken = value;

    user.save((err, user) => {
      if (err) {
        return next(err)
      }

      res.status(200).json({accessToken: user.accessToken.value});
    });
  });
};