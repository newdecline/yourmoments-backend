const ThroughElements = require('../models/ThroughElements');

exports.getThroughElements = async (req, res, next) => {
  const _throughElements = await ThroughElements.findOne({}).limit(1);

  if (!_throughElements) {
    const newThroughElements = new ThroughElements({});
    await newThroughElements.save();
  }

  try {
    const throughElements = await ThroughElements.findOne({}) || {};
    return res.status(200).json(throughElements);
  } catch (err) {
    return next(err)
  }
};

exports.updateThroughElements = async (req, res, next) => {
  const _throughElements = await ThroughElements.findOne({});

  try {
    const throughElements = await ThroughElements.findOneAndUpdate({_id: _throughElements._id},
      req.body,
      {new: true});

    return res.status(200).json(throughElements);
  } catch (err) {
    return next(err)
  }
};