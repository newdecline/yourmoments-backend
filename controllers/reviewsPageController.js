const Reviews = require('../models/Reviews');
const {Image} = require('../models/Image');
const fs = require('fs');
const multer = require('multer');
const {cropImage} = require('../helpers/cropImage');
const {REVIEWS_IMAGE_PATH} = require('../constants');
const isDirectoryExists = require('../helpers/isDirectoryExists');

//Создать папку для картинок под фотосерии если ее нет
fs.mkdirSync(`.${REVIEWS_IMAGE_PATH}`, {recursive: true}, (err) => {
  if (err) throw err;
});

isDirectoryExists(`.${REVIEWS_IMAGE_PATH}`);

//Какие типы картинок разрешены
const allowedMimeTypes = ['image/jpeg'];

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `.${REVIEWS_IMAGE_PATH}`);
  },
  filename: function (req, file, cb) {
    const fileName = `${file.fieldname}-${new Date().getTime().toString()}.jpg`;

    cb(null, fileName);
  }
});

const fileFilter = (req, file, cb) => {
  if (allowedMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    return cb({message: 'Формат файла не верный'}, false);
  }
};

const upload = multer({
  storage,
  limits: {
    fileSize: 1024 * 1024 * 15
  },
  fileFilter
}).any();


exports.getReviews = async (req, res, next) => {
  try {

    const reviews = await Reviews.find(req.query).sort([['position']]).exec();

    return res.status(200).json(reviews);
  } catch (err) {
    return next(err);
  }
};

exports.createReviews = async (req, res, next) => {
  try {
    const reviews = new Reviews(req.body);
    await reviews.save();

    return res.status(200).json(reviews);
  } catch (err) {
    return next(err);
  }
};

exports.updateReviews = async (req, res, next) => {
  try {
    const reviews = await Reviews.findByIdAndUpdate(req.body._id, req.body, {new: true});

    return res.status(200).json(reviews);
  } catch (err) {
    return next(err);
  }
};

exports.updateImageReviews = async (req, res, next) => {
  upload(req, res, async function (err) {
    if (err) {
      return next(err)
    }

    const {_id} = req.query;
    const originalImageFile = req.files[0];
    const {originalImageId, croppedImage} = req.body;

    const ifNeedUpdate = !!croppedImage.cropperData;

    let update = {};

    //Полный путь где лежит оригинал картинки
    const originalImagePath = (originalImageFile && `./${originalImageFile.path}`) || `.${req.body.originalImage}`;

    //Если нужно удалить оригинальную картинку фотосерии
    if (originalImageFile && originalImageId) {
      try {
        await Image.deleteImage(originalImageId);
      } catch (e) {
        console.log(e);
      }
    }

    //Если нужно созать новую оригинальную картинку фотосерии
    if (originalImageFile && !req.body.originalImage) {
      try {
        const newOriginalImage = await Image.saveImage(`${REVIEWS_IMAGE_PATH}/${originalImageFile.filename}`, originalImageFile.filename);
        update = {
          ...update,
          originalImage: newOriginalImage
        };
      } catch (e) {
        console.log(e);
      }
    }

    //Если нужно обновить или создать обрезанную картинку
    if (croppedImage.cropperData) {
      const imageName = `croppedImage-${new Date().getTime().toString()}.jpg`;
      const croppedImagePath = `${REVIEWS_IMAGE_PATH}/${imageName}`;
      const {width, height, x, y} = croppedImage.cropperData;

      if (croppedImage.imageId) {
        try {
          await Image.deleteImage(croppedImage.imageId);
        } catch (e) {
          console.log(e);
        }
      }

      try {
        await cropImage(
          originalImagePath,
          width,
          height,
          x,
          y,
          `.${croppedImagePath}`);
      } catch (e) {
        console.error(e);
      }


      try {
        const newCroppedImage = await Image.saveImage(croppedImagePath, imageName);
        update = {
          ...update,
          croppedImage: newCroppedImage
        };
      } catch (e) {
        console.log(e);
      }
    }

    try {
      let reviews;

      if (ifNeedUpdate) {
        reviews = await Reviews.findByIdAndUpdate(_id, update, {new: true});
      } else {
        reviews = await Reviews.findById(_id);
      }

      return res.status(200).json(reviews);
    } catch (err) {
      return next(err);
    }
  });
};

exports.moveReviews = async (req, res, next) => {
  const reviews = req.body;

  try {
    for (let i = 0; i < reviews.length; i++) {
      await Reviews.findByIdAndUpdate(reviews[i]._id, {
        position: reviews[i].position
      })
    }

    const sortedReviews = await Reviews.find({}).sort([['position']]).exec();

    return res.status(200).json(sortedReviews);
  } catch (err) {
    return next(err);
  }
};

exports.deleteReviews = async (req, res, next) => {
  try {
    const review = await Reviews.find(req.query);

    const idImages = [
      review[0].originalImage.imageId,
      review[0].croppedImage.imageId,
    ].filter(item => item);

    //Удалить картинки связанные с отзывом
    for (let i = 0; i < idImages.length; i++) {
      try {
        await Image.deleteImage(idImages[i]);
      } catch (e) {
        console.error(e);
      }
    }

    await Reviews.findOneAndDelete(req.query);

    const reviews = await Reviews.find({});

    return res.status(200).json(reviews);
  } catch (err) {
    return next(err);
  }
};