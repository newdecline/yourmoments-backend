const TrainingPage = require('../models/TrainingPage');
const Lesson = require('../models/Lesson');

exports.getTrainingPage = async (req, res, next) => {
  const trainingPage = await TrainingPage.findOne({});
  if(!trainingPage) {
    const newTrainingPage = new TrainingPage({});
    await newTrainingPage.save();
  }

  try {
    const trainingPage = await TrainingPage.findOne({}) || {};
    return res.status(200).json(trainingPage);
  } catch (err) {
    return next(err)
  }
};

exports.updateTrainingPage = async (req, res, next) => {
  const {_id} = req.body;

  try {
    const trainingPage = await TrainingPage.findOneAndUpdate({_id}, req.body, {new: true});

    return res.status(200).json(trainingPage);
  } catch (err) {
    return next(err)
  }
};

exports.getLessons = async (req, res, next) => {
  try {
    const lessons = await Lesson.find(req.query).sort([['position']]).exec();

    return res.status(200).json(lessons);
  } catch (err) {
    return next(err)
  }
};

exports.createLessons = async (req, res, next) => {
  try {
    const newLessons = new Lesson(req.body);
    await newLessons.save();

    return res.status(200).json(newLessons);
  } catch (err) {
    return next(err)
  }
};

exports.updateLesson = async (req, res, next) => {
  const {_id} = req.body;

  try {
    const updatedLesson = await Lesson.findOneAndUpdate({_id}, req.body, {new: true});

    return res.status(200).json(updatedLesson);
  } catch (err) {
    return next(err)
  }
};

exports.moveLesson = async (req, res, next) => {
  const lessons = req.body;

  try {
    for (let i = 0; i < lessons.length; i++) {
      await Lesson.findByIdAndUpdate(lessons[i]._id, {
        position: lessons[i].position
      })
    }

    const sortedLessons = await Lesson.find({}).sort([['position']]).exec();

    return res.status(200).json(sortedLessons);
  } catch (err) {
    return next(err)
  }
};

exports.updateHideLesson = async (req, res, next) => {
  const {_id, hide} = req.body;

  try {
    await Lesson.findOneAndUpdate({_id}, {hide});
    const sortedLessons = await Lesson.find({}).sort([['position']]).exec();

    return res.status(200).json(sortedLessons);
  } catch (err) {
    return next(err)
  }
};

exports.deleteLessons = async (req, res, next) => {
  try {
    await Lesson.findOneAndDelete(req.body);
    const lessons = await Lesson.find({});

    return res.status(200).json(lessons);
  } catch (err) {
    return next(err)
  }
};