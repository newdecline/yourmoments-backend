const User  = require('../models/User');
const AccessToken = require('./../models/AccessToken');

exports.logoutController = async (req, res, next) => {
  const loggedInUser = res.locals.loggedInUser;
  const loggedInUserId = loggedInUser._id;
  const accessTokenId = loggedInUser.accessToken;

  try {
    await AccessToken.findOneAndDelete({_id: accessTokenId});
    await User.findOneAndUpdate({_id: loggedInUserId}, {accessToken: null});

    return res.status(200).json({logout: true});
  } catch (err) {
    return next(err)
  }
};