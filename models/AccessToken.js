const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    value : {
        type: String,
        required: true,
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    }
});

const AccessToken = mongoose.model('AccessToken', schema);

module.exports = AccessToken;