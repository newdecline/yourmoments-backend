const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const fs = require('fs');

const imageSchema = new Schema({
  path: String,
  name: String,
  imageId: { type: Schema.ObjectId, auto: true }
});

imageSchema.statics.saveImage = async function (destinationPath, name) {
  const newImage = new Image({
    path: destinationPath,
    name: name
  });

  return await newImage.save();
};

imageSchema.statics.deleteImage = async function (imageId) {
  const doc = await this.findOne({imageId});
  try {
    fs.unlinkSync(`.${doc.path}`)
  } catch(err) {
    console.error(err)
  }

  await this.findOneAndDelete({imageId});
};

const Image = mongoose.model('Image', imageSchema);

module.exports = {imageSchema, Image};
