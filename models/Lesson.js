const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    default: 'Имя модуля'
  },
  title: {
    type: String,
    default: 'Заголовок'
  },
  subtitle: {
    type: String,
    default: 'Подзаголовок'
  },
  descriptionHTML: {
    type: String,
    default: '<ul><li>Описание</li></ul>'
  },
  position: {
    type: Number,
    default: 0
  },
  hide: {
    type: Boolean,
    default: true
  }
}, {autoCreate: true});

const Lesson = mongoose.model('Lesson', schema);

module.exports = Lesson;