const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  title: {
    type: String,
    default: 'Заголовок'
  },
  subtitle1HTML: {
    type: String,
    default: "<p>Подзаголовок-1</p>"
  },
  subtitle2HTML: {
    type: String,
    default: "<p>Подзаголовок-2</p>"
  },
  costOfAllModules: {
    type: String,
    default: '5 000'
  },
  note: {
    type: String,
    default: 'Примечание'
  }
}, {autoCreate: true});

const TrainingPage = mongoose.model('TrainingPage', schema);

module.exports = TrainingPage;