const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  duration: String,
  serviceName1: String,
  cost1: String,
  resultsHTML1: String,
  serviceName2: String,
  cost2: String,
  resultsHTML2: String,
  notesHTML: String,
  surchargeHTML: String,
  position: {
    type: Number,
    default: 1
  }
}, {autoCreate: true});

schema.statics.createTariff = async function (req) {
  const {
    name,
    position
  } = req.body;

  const newTariff = new Tariff({name, position});

  return await newTariff.save();
};

schema.statics.getTariffs = async function (req) {
  const query = req.query || {};

  return await this.find(query).sort([['position']]).exec();
};

schema.statics.updateTariff = async function (req) {
  const {_id} = req.body;

  return await this.findOneAndUpdate({_id}, req.body, {new: true});
};

schema.statics.deleteTariff = async function (req) {
  const {id} = req.params;

  await this.findOneAndDelete({_id: id});

  return await this.find({});
};

schema.statics.moveTariff = async function (req) {
  const tariffs = req.body;

  for (let i = 0; i < tariffs.length; i++) {
    await this.findByIdAndUpdate(tariffs[i]._id, {
      position: tariffs[i].position
    })
  }

  return await this.find({}).sort([['position']]).exec();
};

const Tariff = mongoose.model('Tariff', schema);

module.exports = Tariff;