const mongoose = require('mongoose');
const {Image} = require('../models/Image');
const {SAVE_IMAGE_PATH} = require('../constants');

const Schema = mongoose.Schema;

const schema = new Schema({
  photoSeries: {
    type: Schema.Types.ObjectId,
    ref: 'PhotoSeries'
  },
  images: {
    type: Array
  }
}, {autoCreate: true});

schema.statics.getImages = async function (req) {
  const {params} = req;
  const galleryId = params.id;

  return await this.findOne({_id: galleryId})
};

schema.statics.addImages = async function (req, fileName) {
  const {params} = req;
  const galleryId = params.id;

  // Создание картинок(ки) в галереи фотосерии
  try {
    const newImage = await Image.saveImage(
      `${SAVE_IMAGE_PATH}/${fileName}`,
      fileName);

    await this.findOneAndUpdate(
      {_id: galleryId},
      {"$push": {images: newImage}});
  } catch (e) {
    console.error(e);
  }
};

schema.statics.deleteImage = async function (req) {
  const {params} = req;
  const galleryId = params.galleryId;
  const imageId = params.imageId;

  // Удаление картиноки в галереи фотосерии
  try {
    await Image.deleteImage(imageId);
  } catch (e) {
    console.error(e);
  }

  return await this.findOneAndUpdate(
    {_id: galleryId},
    {
      "$pull": {
        'images': {'imageId': mongoose.Types.ObjectId(imageId)}
      }
    },
    {new: true});
};

const PhotoSeriesGalley = mongoose.model('PhotoSeriesGalley', schema);

module.exports = PhotoSeriesGalley;