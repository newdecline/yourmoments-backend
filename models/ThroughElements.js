const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  vkontakte: {
    type: String,
    default: 'vkontakte'
  },
  instagram: {
    type: String,
    default: 'instagram'
  },
  email: {
    type: String,
    default: 'email'
  },
  phone1: {
    type: String,
    default: 'phone1'
  },
  phone2: {
    type: String,
    default: 'phone2'
  }
}, {autoCreate: true});

const ThroughElements = mongoose.model('ThroughElements', schema);

module.exports = ThroughElements;