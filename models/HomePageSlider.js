const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
  slides: {
    type: Array
  }
}, {autoCreate: true});

const HomePageSlider = mongoose.model('HomePageSlider', schema);

module.exports = HomePageSlider;