const mongoose = require('mongoose');
const {imageSchema, Image} = require('../models/Image');
const PhotoSeriesGalley = require('../models/PhotoSeriesGalley');
const slug = require('mongoose-slug-updater');
mongoose.plugin(slug);
const {cropImage} = require('../helpers/cropImage');
const {SAVE_IMAGE_PATH} = require('../constants');

const Schema = mongoose.Schema;

const schema = new Schema({
  slug: {
    type: String,
    slug: 'name',
    slugPaddingSize: 4,
    unique: true
  },
  name: {
    type: String,
    trim: true,
    required: true
  },
  urlVideo: {
    type: String,
    trim: true
  },
  category: {
    type: String,
    required: true
  },
  originalImage: imageSchema,
  croppedBannerImage: imageSchema,
  croppedCardImage: imageSchema,
  galleryImages: {
    type: Schema.Types.ObjectId,
    ref: 'PhotoSeriesGalley'
  }
}, {autoCreate: true});

schema.statics.createPhotoSeries = async function (name, category) {
  let update = {
    name,
    category
  };

  const newGalleryImages = new PhotoSeriesGalley({});

  try {
    await newGalleryImages.save();
  } catch (e) {
    console.error(3);
  }

  update = {
    ...update,
    galleryImages: newGalleryImages
  };

  const newPhotoSeries = new PhotoSeries(update);

  return await newPhotoSeries.save();
};

schema.statics.getPhotoSeries = async function (query = {}) {
  return await this.find(query);
};

schema.statics.updatePhotoSeries = async function (req) {
  const {slug} = req.query;
  const originalImageFile = req.files[0];

  const {
    name,
    urlVideo,
    category,
    originalImageId,
    croppedBannerImage,
    croppedCardImage
  } = req.body;

  let update = {
    name,
    category,
    urlVideo
  };

  //Полный путь где лежит оригинал картинки
  const originalImagePath = (originalImageFile && `./${originalImageFile.path}`) || `.${req.body.originalImage}`;

  //Если нужно удалить оригинальную картинку фотосерии
  if (originalImageFile && originalImageId) {
    try {
      await Image.deleteImage(originalImageId);
    } catch (e) {
      console.log(e);
    }
  }

  //Если нужно созать новую оригинальную картинку фотосерии
  if (originalImageFile && !req.body.originalImage) {
    try {
      const newOriginalImage = await Image.saveImage(`${SAVE_IMAGE_PATH}/${originalImageFile.filename}`, originalImageFile.filename);
      update = {
        ...update,
        originalImage: newOriginalImage
      };
    } catch (e) {
      console.log(e);
    }
  }

  //Если нужно обновить или создать обрезанную картинку в баннере фотосерии
  if (croppedBannerImage.cropperData) {

    const imageName = `croppedBannerImage-${new Date().getTime().toString()}.jpg`;
    const croppedBannerImagePath = `${SAVE_IMAGE_PATH}/${imageName}`;
    const {width, height, x, y} = croppedBannerImage.cropperData;

    if (croppedBannerImage.imageId) {
      try {
        await Image.deleteImage(croppedBannerImage.imageId);
      } catch (e) {
        console.log(e);
      }
    }

    try {
      await cropImage(
        originalImagePath,
        width,
        height,
        x,
        y,
        `.${croppedBannerImagePath}`);
    } catch (e) {
      console.error(e);
    }


    try {
      const newCroppedBannerImage = await Image.saveImage(croppedBannerImagePath, imageName);
      update = {
        ...update,
        croppedBannerImage: newCroppedBannerImage
      };
    } catch (e) {
      console.log(e);
    }
  }

  //Если нужно обновить или создать обрезанную картинку в для карточки фотосерии
  if (croppedCardImage.cropperData) {
    const imageName = `croppedCardImage-${new Date().getTime().toString()}.jpg`;
    const croppedCardImagePath = `${SAVE_IMAGE_PATH}/${imageName}`;
    const {width, height, x, y} = croppedCardImage.cropperData;

    if (croppedCardImage.imageId) {
      try {
        await Image.deleteImage(croppedCardImage.imageId);
      } catch (e) {
        console.log(e);
      }
    }

    try {
      await cropImage(
        originalImagePath,
        width,
        height,
        x,
        y,
        `.${croppedCardImagePath}`);
    } catch (e) {
      console.error(e);
    }

    try {
      const newCroppedCardImage = await Image.saveImage(croppedCardImagePath, imageName);
      update = {
        ...update,
        croppedCardImage: newCroppedCardImage
      };
    } catch (e) {
      console.log(e);
    }
  }

  return await this.findOneAndUpdate(
    {slug},
    update,
    {new: true});
};

schema.statics.deletePhotoSeries = async function (slug) {
  const photoSeries = await this.findOne(slug);

  const photoSeriesGallery = await PhotoSeriesGalley.findOne({'_id': mongoose.Types.ObjectId(photoSeries.galleryImages)});

  const galleryImages = photoSeriesGallery.images;

  //Удаление всех картинок связанных с галерей и удаление самой гелереи
  photoSeriesGallery.remove();
  if (galleryImages.length !== 0) {
    for (let i = 0; i < galleryImages.length; i++) {
      try {
        await Image.deleteImage(galleryImages[i].imageId);
      } catch (e) {
        console.error(e);
      }
    }
  }

  const hasImages = photoSeries.originalImage || photoSeries.croppedBannerImage || photoSeries.croppedCardImage;

  if (hasImages) {
    const idImages = [
      photoSeries.originalImage.imageId,
      photoSeries.croppedBannerImage.imageId,
      photoSeries.croppedCardImage.imageId
    ];

    //Удалить картинку под баннер, карточку и оригинал для них
    for (let i = 0; i < idImages.length; i++) {
      try {
        await Image.deleteImage(idImages[i]);
      } catch (e) {
        console.error(e);
      }
    }
  }


  try {
    await this.findOneAndDelete(slug);
  } catch (e) {
    console.error(e);
  }

  return await this.find({category: photoSeries.category});
};

const PhotoSeries = mongoose.model('PhotoSeries', schema);

module.exports = PhotoSeries;