const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  email: {
    type: String,
    trim: true,
    required: 'Email is required'
  },
  password: {
    type: String,
    required: 'Password is required'
  },
  accessToken: {
    type: Schema.Types.ObjectId,
    ref: 'AccessToken'
  }
});

const User = mongoose.model('User', schema);

module.exports = User;