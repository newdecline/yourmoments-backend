const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    default: 'Имя автора отзыва'
  },
  descriptionHTML: {
    type: String,
    default: '<p>Текст отзыва</p>'
  },
  originalImage: {
    type: Object,
    default: {}
  },
  croppedImage: {
    type: Object,
    default: {}
  },
  position: {
    type: Number,
    default: 0
  },
  hide: {
    type: Boolean,
    default: false
  }
}, {autoCreate: true});

const Reviews = mongoose.model('Reviews', schema);

module.exports = Reviews;