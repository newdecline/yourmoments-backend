module.exports = isDirectoryExists = directory => {
  try {
    fs.statSync(directory);
    return true;
  } catch (e) {
    return false;
  }
};