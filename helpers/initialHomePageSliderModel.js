const HomePageSlider = require('../models/HomePageSlider');


const initialHomePageSliderModel = async () => {
  const slider = await HomePageSlider.find();

  if (slider.length === 0) {
    const newSlider = new HomePageSlider({});
    await newSlider.save();
  }
};

module.exports = {initialHomePageSliderModel};