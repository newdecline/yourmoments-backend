const gm = require('gm');

const cropImage = (inputImage, width, height, left, top, outputImage) =>
  new Promise((resolve, reject) => {
    gm(inputImage)
      .crop(Math.ceil(width), Math.ceil(height), Math.ceil(left), Math.ceil(top))
      .write(outputImage, (err) => {
        if (err) {
          return reject(err)
        }
        return resolve(true)
      });
  });

module.exports = {cropImage};
