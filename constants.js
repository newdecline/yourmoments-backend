module.exports = {
  SAVE_IMAGE_PATH: '/uploads/photo-series', //Путь куда сохранять картинки для фотосерий
  REVIEWS_IMAGE_PATH: '/uploads/reviews', //Путь куда сохранять картинки для отзывов
  HOMEPAGE_BANNER_IMAGE_PATH: '/uploads/homepage-banner', //Путь куда сохранять картинки для баннера на гланой странице
};