import React, {useEffect} from "react";
import {observer} from "mobx-react";
import {Redirect, Route, Switch} from "react-router";
import {authStore} from "../mobx/authStore";
import {routesConfig} from "./routesConfig";
import {customHistory} from "../customHistory";
import {Router} from "react-router-dom";
import {AuthPage} from "../pages/AuthPage/AuthPage";
import {useAlert} from "react-alert";

export const RootRouter = observer(() => {
  const {authToken} = authStore;

  const ConfiguredRoute = ({path, exact, layout: {component: Layout, props: layoutProps}}) => {
    return (
      <Route
        path={path}
        exact={exact}
        render={renderProps => <Layout path={path} {...layoutProps} {...renderProps} />}/>
    );
  };

  const renderRoutes = () => {
    if (!authToken) {
      return <Switch>
        <Route component={AuthPage}/>
      </Switch>
    } else {
      return <Switch>
        {routesConfig.map((routeProps, i) => <ConfiguredRoute {...routeProps} key={i}/>)}
        <Redirect to={'/'}/>
      </Switch>
    }
  };

  const alert = useAlert();

  useEffect(() => {
    window.reactAlert = alert;
  }, []);

  return (
    <Router history={customHistory}>
      {renderRoutes()}
    </Router>
  )
});
