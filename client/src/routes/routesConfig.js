import React from "react";
import {IndexPage} from "./../pages/IndexPage";
import {MainLayout} from "./../components/layouts/MainLayout";
import {PortfolioPage} from "./../pages/content/portfolio-page/PortfolioPage";
import {PricePage} from "./../pages/content/price-page/PricePage";
import {ReviewsPage} from "./../pages/content/reviews-page/ReviewsPage";
import {ReviewsUpdatePage} from "./../pages/content/reviews-update-page/ReviewsUpdatePage";
import {SeriesUpdatePage} from "./../pages/content/photo-series-update-page/SeriesUpdatePage";
import {TariffPage} from "./../pages/content/tariff-page/TariffPage";
import {PhotoSeriesListPage} from "./../pages/content/portfolio-page/PhotoSeriesListPage/PhotoSeriesListPage";
import {TrainingPage} from "./../pages/content/training-page/TrainingPage";
import {LessonUpdatePage} from "./../pages/content/lesson-update-page/LessonUpdatePage";
import {ThroughElements} from "./../pages/ThroughElements";

import HomeIcon from '@material-ui/icons/Home';
import WorkIcon from '@material-ui/icons/Work';
import PeopleIcon from '@material-ui/icons/People';
import BusinessIcon from '@material-ui/icons/Business';
import EmojiEmotionsIcon from '@material-ui/icons/EmojiEmotions';
import MoneyIcon from '@material-ui/icons/Money';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import RateReviewIcon from '@material-ui/icons/RateReview';
import {HomePage} from "../pages/content/home-page/HomePage";

export const routesConfig = [
  {
    path: '/',
    as: '/',
    exact: true,
    layout: {
      component: MainLayout,
      props: {
        content: IndexPage,
      },
    }
  },
  {
    path: '/content',
    as: '/content',
    layout: {
      component: MainLayout,
      props: {
        content: ThroughElements,
        drawer: [
          {
            path: '/home-page',
            as: '/home-page',
            exact: true,
            label: 'Главная страница',
            page: HomePage,
            icon: <HomeIcon/>,
          },
          {
            path: '/portfolio-page',
            as: '/portfolio-page',
            exact: true,
            label: 'Портфолио',
            page: PortfolioPage,
            icon: <WorkIcon/>,
            routing: false,
            children: [
              {
                path: '/categories/:categoryName',
                as: '/categories/paired',
                label: 'Парные серии',
                page: PhotoSeriesListPage,
                icon: <PeopleIcon/>,
              },
              {
                path: '/categories/:categoryName',
                as: '/categories/wedding',
                label: 'Свадебные серии',
                page: PhotoSeriesListPage,
                icon: <EmojiEmotionsIcon/>,
              },
              {
                path: '/categories/:categoryName',
                as: '/categories/business-content',
                label: 'Контент для бизнеса',
                page: PhotoSeriesListPage,
                icon: <BusinessIcon/>,
              }
            ]
          },
          {
            path: '/photo-series-update',
            as: '/photo-series-update',
            page: SeriesUpdatePage,
            hidden: true,
          },
          {
            path: '/price-page',
            as: '/price-page',
            exact: true,
            label: 'Цены',
            page: PricePage,
            icon: <MoneyIcon/>,
          },
          {
            path: '/tariff-page/:id',
            page: TariffPage,
            hidden: true,
          },
          {
            path: '/training-page',
            as: '/training-page',
            exact: true,
            label: 'Обучение',
            page: TrainingPage,
            icon: <TrendingUpIcon/>,
          },
          {
            path: '/lesson-update-page/:id',
            as: '/lesson-update-page',
            page: LessonUpdatePage,
            hidden: true,
          },
          {
            path: '/reviews-page',
            as: '/reviews-page',
            exact: true,
            label: 'Отзывы',
            page: ReviewsPage,
            icon: <RateReviewIcon/>,
          },
          {
            path: '/reviews-update-page/:id',
            as: '/reviews-update-page',
            page: ReviewsUpdatePage,
            hidden: true,
          },
        ]
      },
    },
  }
];
