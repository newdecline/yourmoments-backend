import React from 'react';
import {transitions, positions, Provider as AlertProvider} from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import {GlobalStyle} from "./globalStyle";
import {Normalize} from 'styled-normalize';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import {RootRouter} from "./routes/RootRouter";
import {routesConfig} from "./routes/routesConfig";

import './css/cropper.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const options = {
  position: positions.TOP_RIGHT,
  timeout: 5000,
  offset: '30px',
  transition: transitions.SCALE,
  containerStyle: {
    zIndex: 9999
  }
};

const commonTheme = createMuiTheme({
  drawerWidth: 240,
});

export const App = () => {
  return (
    <ThemeProvider theme={commonTheme}>
      <AlertProvider template={AlertTemplate} {...options}>
        <Normalize/>
        <GlobalStyle/>
        <div className='app'>
          <RootRouter routesConfig={routesConfig}/>
        </div>
      </AlertProvider>
    </ThemeProvider>
  );
};
