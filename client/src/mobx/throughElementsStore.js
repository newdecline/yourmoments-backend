import React from "react";
import {observable, action} from 'mobx';
import apiService from "../utilities/apiService";
import {AlertMessage} from "../components/AlertMessage";

class throughElementsStore {
  @observable loading = true;
  @observable updating = false;
  @observable throughElements = {};

  @action
  async getThroughElements() {
    this.loading = true;
    const {status, body} = await apiService.getThroughElements();

    if (status === 200) {
      this.throughElements = body;
    }
    this.loading = false;
  };

  @action
  async updateThroughElements(data) {
    this.updating = true;
    const {status, body} = await apiService.updateThroughElements(data);

    if (status === 200) {
      this.throughElements = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }
    this.updating = false;
  };
}

export default new throughElementsStore();