import {action, observable} from 'mobx';
import apiService from "../utilities/apiService";
import {customHistory} from "../customHistory";
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

class PhotoSeriesStore {
  @observable loading = true;
  @observable updating = false;

  @observable updatedPhotoSeries = {};

  @action
  async fetchPhotoSeries(query) {
    this.loading = true;
    const {status, body} = await apiService.getPhotoSeries(query);
    const photoSeries = body[0];

    if (status === 200) {
      this.updatedPhotoSeries = {
        ...photoSeries
      };
      this.loading = false;
    }
  };

  @action
  async updatePhotoSeries(data, slug) {
    this.updating = true;

    const {status, body} = await apiService.putPhotoSeries(data, slug);

    if (status === 200) {
      this.updatedPhotoSeries = body;
      customHistory.push(`/content/photo-series-update?slug=${body.slug}`);
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updating = false;
  };
}

export default new PhotoSeriesStore();