import {observable, action} from 'mobx';
import apiService from "../utilities/apiService";
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

class HomePageSliderStore {
  @observable loading = true;
  @observable updating = false;
  @observable openPopup = false;

  @observable slides = [];

  @action
  setOpenPopup(bool) {
    this.openPopup = bool
  }

  @action
  async getSlides(data) {
    this.loading = true;

    const {status, body} = await apiService.getSlides(data);

    if (status === 200) {
      this.slides = body || [];
    }

    this.loading = false;
  }

  @action
  async deleteSlide(data) {
    this.updating = true;
    const {status, body} = await apiService.deleteSlide(data);

    if (status === 200) {
      this.slides = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updating = false;
  }

  @action
  async updateSlide(data) {
    this.updating = true;

    const {status, body} = await apiService.updateSlide(data);

    if (status === 200) {
      this.slides = body || [];
      this.openPopup = false;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updating = false;
  }

  @action
  async moveSlide(data) {
    this.updating = true;
    const {status, body} = await apiService.moveSlide(data);

    if (status === 200) {
      this.slides = body || [];
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }
    this.updating = false;
  }
}

export default new HomePageSliderStore();