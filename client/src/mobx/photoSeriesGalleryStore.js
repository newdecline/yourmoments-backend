import {action, observable} from 'mobx';
import apiService from "../utilities/apiService";
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

class PhotoSeriesGalleryStore {
  @observable loading = true;
  @observable updating = false;

  @observable photoSeriesGallery = {};

  @action
  async fetchPhotoSeriesGallery(galleryId) {
    this.loading = true;
    const {status, body} = await apiService.fetchPhotoSeriesGallery(galleryId);

    if (status === 200) {
      this.photoSeriesGallery = body;
    }
    this.loading = false;
  };

  @action
  async addImagesToPhotoSeriesGallery(data) {
    this.updating = true;
    const {status, body} = await apiService.addImagesToPhotoSeriesGallery(data);

    if (status === 200) {
      this.photoSeriesGallery = {
        galleryId: body._id,
        images: body.images
      };
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }
    this.updating = false;
  };

  @action
  async deleteImagesFromPhotoSeriesGallery(galleryId, imageId) {
    this.updating = true;
    const {status, body} = await apiService.deleteImagesFromPhotoSeriesGallery(galleryId, imageId);

    if (status === 200) {
      this.photoSeriesGallery = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }
    this.updating = false;
  };
}

export default new PhotoSeriesGalleryStore();