import {observable, action} from 'mobx';
import apiService from "../utilities/apiService";
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

class ReviewsStore {
  @observable loading = true;
  @observable updating = false;

  @observable reviews = [];

  @action
  async getReviews(data) {
    this.loading = true;
    const {status, body} = await apiService.getReviews(data);

    if (status === 200) {
      this.reviews = body;
    }

    this.loading = false;
  }

  @action
  async deleteReviews(data) {
    this.updating = true;
    const {status, body} = await apiService.deleteReviews(data);

    if (status === 200) {
      this.reviews = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updating = false;
  }

  @action
  async updateReviews(data) {
    this.updating = true;

    const {status, body} = await apiService.updateReviews(data);

    if (status === 200) {
      this.reviews[0] = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updating = false;
  }

  @action
  async updateImageReviews(data) {
    this.updating = true;

    const {status, body} = await apiService.updateImageReviews(data);

    if (status === 200) {
      this.reviews[0] = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updating = false;
  }

  @action
  async updatePositionReviews(data) {
    const oldReviews = [...this.reviews];

    const updatedReview = this.reviews.find(review => review._id === data.id);
    const indexUpdatedReview = this.reviews.findIndex(review => review._id === data.id);

    const updatedPositionReviews = oldReviews.map((review, i) => {
      return {...review, position: i}
    });

    if (data.position === 'down') {
      if (!updatedPositionReviews[indexUpdatedReview + 1]) return;

      updatedPositionReviews[indexUpdatedReview] = {
        ...updatedReview,
        position: updatedPositionReviews[indexUpdatedReview].position + 1
      };

      updatedPositionReviews[indexUpdatedReview + 1] = {
        ...updatedPositionReviews[indexUpdatedReview + 1],
        position: updatedPositionReviews[indexUpdatedReview + 1].position - 1
      };
    }

    if (data.position === 'up') {
      if (!updatedPositionReviews[indexUpdatedReview - 1]) return;

      updatedPositionReviews[indexUpdatedReview] = {
        ...updatedReview,
        position: updatedPositionReviews[indexUpdatedReview].position - 1
      };

      updatedPositionReviews[indexUpdatedReview - 1] = {
        ...updatedPositionReviews[indexUpdatedReview - 1],
        position: updatedPositionReviews[indexUpdatedReview - 1].position + 1
      };
    }

    updatedPositionReviews.sort((a, b) => a.position - b.position);

    this.updating = true;

    const {status, body} = await apiService.moveReviews(updatedPositionReviews);

    if (status === 200) {
      this.reviews = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updating = false;
  }
}

export default new ReviewsStore();