import {observable, action} from 'mobx';
import apiService from '../utilities/apiService';
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

export const authStore = observable({
  loading: false,
  authToken: JSON.parse(localStorage.getItem('accessToken')) || null,

  async login(dataValues) {
    this.loading = true;

    const {status, body} = await apiService.login(dataValues);
    if (status === 401) {
      window.reactAlert.info(<AlertMessage message='Нверный логин или пароль'/>);
    } else if (status === 200) {
      localStorage.setItem('accessToken', JSON.stringify(body.accessToken));
      this.authToken = JSON.parse(localStorage.getItem('accessToken'));
    } else {
      window.reactAlert.error(<AlertMessage message='Что то пошло не так, обратитесь к разработчикам'/>);
    }
    this.loading = false;
  },
  autoLogout() {
    localStorage.removeItem('accessToken');
    this.authToken = null;
  },
  async logout() {
    apiService.logout();
    localStorage.removeItem('accessToken');
    this.authToken = null;
  }
}, {
  login: action,
  logout: action
});