import {observable, action} from 'mobx';
import apiService from "../utilities/apiService";
import React from "react";
import {AlertMessage} from "../components/AlertMessage";

class TrainingPageStore {
  @observable loadingPageData = true;
  @observable updatingPageData = false;
  @observable pageData = {};

  @action
  async getTrainingPage() {
    this.loadingPageData = true;
    const {status, body} = await apiService.getTrainingPage();

    if (status === 200) {
      this.pageData = body;
    }
    this.loadingPageData = false;
  };

  @action
  async updateTrainingPage(data) {
    this.updatingPageData = true;
    const {status, body} = await apiService.updateTrainingPage(data);

    if (status === 200) {
      this.pageData = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }
    this.updatingPageData = false;
  };
}

export default new TrainingPageStore();