import {action, observable} from 'mobx';
import apiService from "../utilities/apiService";
import photoSeriesStore from "../mobx/photoSeriesStore";
import photoSeriesGalleryStore from "../mobx/photoSeriesGalleryStore";
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

class PhotoSeriesListStore {
  @observable loading = true;

  @observable photoSeriesList = [];

  @action
  async fetchPhotoSeriesByCategory(query) {
    this.loading = true;
    const {status, body} = await apiService.getPhotoSeries(query);

    if (status === 200) {
      this.photoSeriesList = body;
      photoSeriesStore.updatedPhotoSeries = {};
      photoSeriesGalleryStore.photoSeriesGallery = {}
    }

    this.loading = false;
  };

  @action
  async deletePhotoSeries(slug) {
    this.loading = true;
    const {status, body} = await apiService.deletePhotoSeries(slug);

    if (status === 200) {
      this.photoSeriesList = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.loading = false;
  };
}

export default new PhotoSeriesListStore();