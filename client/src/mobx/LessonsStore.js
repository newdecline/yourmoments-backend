import {observable, action} from 'mobx';
import apiService from "../utilities/apiService";
import React from "react";
import {AlertMessage} from "../components/AlertMessage";

class LessonsStore {
  @observable loadingLessons = true;
  @observable updatingLessons = false;

  @observable lessonsData = [];
  @observable lessonData = {};

  @action
  async getLessonsData() {
    this.loadingLessons = true;
    const {status, body} = await apiService.getLessonsData();

    if (status === 200) {
      this.lessonsData = body;
    }

    this.loadingLessons = false;
  };

  @action
  async getLessonData(data) {
    this.loadingLessons = true;
    const {status, body} = await apiService.getLessonsData(data);

    if (status === 200) {
      this.lessonData = body[0];
    }

    this.loadingLessons = false;
  };

  @action
  async moveLesson(data) {
    this.updatingLessons = true;
    const {status, body} = await apiService.moveLesson(data);

    if (status === 200) {
      this.lessonsData = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updatingLessons = false;
  };

  @action
  async updateLesson(data) {
    this.updatingLessons = true;
    const {status, body} = await apiService.updateLesson(data);

    if (status === 200) {
      this.lessonData = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updatingLessons = false;
  };

  @action
  async updateHideLesson(data) {
    this.updatingLessons = true;
    const {status, body} = await apiService.updateHideLesson(data);

    if (status === 200) {
      this.lessonsData = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updatingLessons = false;
  };

  @action
  async deleteLessons(data) {
    this.updatingLessons = true;
    const {status, body} = await apiService.deleteLessons(data);

    if (status === 200) {
      this.lessonsData = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }

    this.updatingLessons = false;
  };
}

export default new LessonsStore();