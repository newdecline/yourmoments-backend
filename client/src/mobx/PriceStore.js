import {observable, action} from 'mobx';
import apiService from "../utilities/apiService";
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

class PriceStore {
  @observable loadingTariffList = true;
  @observable tariffList = [];

  @observable loadingTariff = true;
  @observable tariff = {};

  @action
  async getTariffs(data) {
    this.loadingTariffList = true;
    const {status, body} = await apiService.getTariffs(data);

    if (status === 200) {
      this.tariffList = body;
      this.tariff = {};
    }
    this.loadingTariffList = false;
  };

  @action
  async getTariff(data) {
    this.loadingTariff = true;
    const {status, body} = await apiService.getTariffs(data);

    if (status === 200) {
      this.tariff = body[0];
    }
    this.loadingTariff = false;
  };

  @action
  async updateTariff(data) {
    this.loadingTariff = true;
    const {status, body} = await apiService.updateTariff(data);

    if (status === 200) {
      this.tariff = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }
    this.loadingTariff = false;
  };

  @action
  async deleteTariff(data) {
    this.loadingTariffList = true;
    const {status, body} = await apiService.deleteTariff(data);

    if (status === 200) {
      this.tariffList = body;
    }
    this.loadingTariffList = false;
  };

  @action
  async moveTariff(data) {
    this.loadingTariffList = true;
    const {status, body} = await apiService.moveTariff(data);

    if (status === 200) {
      this.tariffList = body;
      window.reactAlert.success(<AlertMessage message='Изменения сохранены'/>);
    }
    this.loadingTariffList = false;
  };
}

export default new PriceStore();