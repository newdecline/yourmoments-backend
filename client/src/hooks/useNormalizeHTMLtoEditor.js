import {ContentState, convertFromHTML, EditorState} from "draft-js";

export const useNormalizeHTMLtoEditor = html => {
  return EditorState.createWithContent(
    ContentState.createFromBlockArray(
      convertFromHTML(`${html}`)
    )
  )
};