import React, {useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {customHistory} from "../../../../customHistory";
import {observer} from "mobx-react";
import {AddReviewsForm} from "../AddReviewsForm/AddReviewsForm";
import reviewsStore from "../../../../mobx/reviewsStore";

import {blueGrey} from "@material-ui/core/colors";

import {
  Backdrop,
  CircularProgress, Fade,
  IconButton, Modal, Paper,
  Table, TableBody,
  TableCell,
  TableContainer,
  TableHead, TablePagination,
  TableRow,
  Typography
} from "@material-ui/core";

import {Delete, Edit, AddCircleOutline, ArrowDownward, ArrowUpward} from "@material-ui/icons";

export const ReviewsList = observer(() => {
  const classes = useStyles();

  const updating = reviewsStore.updating;
  const loading = reviewsStore.loading;

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleClickAddReviewsBtn = () => {
    setOpen(true);
  };

  const handleClickUpdateReviews = id => {
    customHistory.push(`/content/reviews-update-page/${id}`);
  };

  const handleClickUpdatePositionReviews = data => {
    reviewsStore.updatePositionReviews(data);
  };

  const handleClickDeleteReviews = id => {
    reviewsStore.deleteReviews({_id: id});
  };

  const rows = reviewsStore.reviews;

  useEffect(() => {
    reviewsStore.getReviews();
  }, []);

  return <div className={classes.root}>
    {loading
      ? <CircularProgress className={classes.progress}/>
      : <Paper className={classes.paper}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map(column => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{borderBottom: `1px solid ${blueGrey[900]}`}}
                  >
                    <Typography variant="h6">
                      Добавить отзыв
                      <IconButton
                        onClick={handleClickAddReviewsBtn}
                        style={{
                          marginLeft: 10,
                          color: blueGrey[900]
                        }}
                        size={"medium"}
                        aria-label="add"><AddCircleOutline/></IconButton>
                    </Typography>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>

            <TableBody>
              <TableRow>
                {columns.map(column => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{minWidth: column.minWidth, fontWeight: 700}}>
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
              {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, i) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={`${row._id}${i}`}>
                    {columns.map(column => {
                      const value = row[column.id];

                      return (
                        <TableCell
                          key={row._id}
                          align={column.align}
                          style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between'
                          }}>
                          {value}
                          <div>
                            <IconButton
                              disabled={updating}
                              aria-label="move up"
                              size={'medium'}
                              onClick={() => handleClickUpdatePositionReviews({
                                id: row._id,
                                position: 'up'
                              })}>
                              <ArrowUpward/>
                            </IconButton>
                            <IconButton
                              disabled={updating}
                              aria-label="move down"
                              size={'medium'}
                              onClick={() => handleClickUpdatePositionReviews({
                                id: row._id,
                                position: 'down'
                              })}>
                              <ArrowDownward/>
                            </IconButton>
                            <IconButton
                              disabled={updating}
                              aria-label="add"
                              size={'medium'}
                              onClick={() => handleClickUpdateReviews(row._id)}>
                              <Edit/>
                            </IconButton>
                            <IconButton
                              disabled={updating}
                              aria-label="add"
                              size={'medium'}
                              onClick={() => handleClickDeleteReviews(row._id)}>
                              <Delete/>
                            </IconButton>
                          </div>
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>

        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>}

    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}>
      <Fade in={open}>
        <>
          <AddReviewsForm/>
        </>
      </Fade>
    </Modal>
  </div>
});

const columns = [
  {
    id: 'name',
    label: 'Автор отзыва',
    minWidth: 170,
    align: 'left'
  }
];

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    '&:focus': {
      outline: 'none'
    }
  },
  button: {
    margin: '10px 0',
    color: '#000',
    '&:hover': {
      backgroundColor: blueGrey[400],
      color: '#000'
    }
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    '&:focus': {
      outline: 'none'
    }
  },
  progress: {
    color: blueGrey[900]
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
}));