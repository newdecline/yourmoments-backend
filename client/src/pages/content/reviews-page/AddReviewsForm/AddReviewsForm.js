import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {Button, CircularProgress, Paper, TextField} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import apiService from "../../../../utilities/apiService";
import {customHistory} from "../../../../customHistory";
import {blueGrey} from "@material-ui/core/colors";

import AddIcon from '@material-ui/icons/Add';
import {maxBy} from "lodash";

export const AddReviewsForm = () => {
  const classes = useStyles();

  const [loading, setLoading] = useState(false);

  const {register, handleSubmit, errors} = useForm();

  const onSubmit = async values => {
    setLoading(true);

    try {
      const lastPosition = await getLastPosition();

      const {status, body} = await apiService.createReviews({...values, position: lastPosition + 1});

      if (status === 200) {
        customHistory.push(`/content/reviews-update-page/${body._id}`);
      }
    } catch (e) {
      console.log(e);
    }
  };

  return <div className={classes.root}>
    <Paper className={classes.paper}>
      {loading
        ? <CircularProgress style={{color: blueGrey[900]}}/>
        : <form onSubmit={handleSubmit(onSubmit)}>
          <TextField
            error={!!errors.name}
            label="Имя автора отзыва"
            style={{margin: '16px 0'}}
            placeholder="Введите имя автора отзыва"
            helperText="Это имя будет использоваться в качестве имени автора отзыва"
            fullWidth
            margin="normal"
            inputRef={register({required: true})}
            name='name'
            InputLabelProps={{
              shrink: true,
            }}/>

          <Button
            disabled={!!errors.name}
            type='submit'
            variant="contained"
            color="primary"
            size="medium"
            className={classes.button}
            startIcon={<AddIcon/>}
          >Добавить отзыв</Button>
        </form>}
    </Paper>
  </div>
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  paper: {
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4),
    '&:focus': {
      outline: 'none'
    }
  }
}));

const getLastPosition = async () => {
  const {status, body} = await apiService.getReviews();

  if (status === 200) {
    if (body.length === 0) {
      return 0
    }

    return maxBy(body, 'position').position
  }
};