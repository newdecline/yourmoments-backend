import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Typography} from "@material-ui/core";
import {ReviewsList} from "./ReviewsList/ReviewsList";

export const ReviewsPage = () => {
  const classes = useStyles();

  return <div className={classes.root}>
    <Typography variant="h4" component="h4" gutterBottom>Редактирование страницы "Отзывы"</Typography>

    <ReviewsList/>
  </div>
};

const useStyles = makeStyles(() => ({
  root: {}
}));