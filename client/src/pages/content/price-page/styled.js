import styled from 'styled-components';

export const StyledPricePage = styled('div')`
  position: relative;
  .droppable-container {
    &.moving {
      background-color: #ccc;
    }
  }
`;