import React, {useState, useEffect} from 'react';
import {observer} from 'mobx-react';
import {NavLink} from "react-router-dom";
import {StyledPricePage} from "./styled";
import priceStore from "../../../mobx/PriceStore";
import {CreateTariffForm} from "../../../components/forms/CreateTariffForm/CreateTariffForm";
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import arrayMove from 'array-move';
import classnames from 'classnames';

import {
  Typography,
  Button,
  Paper,
  Grid,
  Modal,
  Backdrop,
  Fade,
  CircularProgress,
  IconButton
} from "@material-ui/core";

import {makeStyles} from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from '@material-ui/icons/Save';
import FlipToFrontIcon from '@material-ui/icons/FlipToFront';

import {blueGrey} from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  gridContainer: {
    flexGrow: 1,
    '&.moving': {
      backgroundColor: '#ccc'
    }
  },
  paper: {
    height: 140,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: 'background-color .3s',
    '&:hover': {
      backgroundColor: blueGrey[100],
      cursor: 'pointer'
    }
  },
  modalPaper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4),
    '&:focus': {
      outline: 'none'
    }
  },
  gridItem: {
    position: 'relative',
    width: '20%',
  },
  link: {
    display: 'block',
    fontSize: '20px',
    textDecoration: 'none',
    color: '#000',
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progress: {
    color: blueGrey[900]
  },
  trash: {
    position: 'absolute',
    right: 10,
    top: 10,
    zIndex: 1,
    backgroundColor: 'transparent',
    '& .trash-icon': {
      color: blueGrey[300],
    },
    '&:hover': {
      backgroundColor: 'transparent',
      '& .trash-icon': {
        color: blueGrey[700],
      }
    }
  },
}));

export const PricePage = observer(() => {
  const classes = useStyles();
  const loading = priceStore.loadingTariffList;
  const tariffs = priceStore.tariffList;

  const [open, setOpen] = useState(false);

  const [tariffsOrdered, setTariffsOrdered] = useState(tariffs || []);

  const [allowMove, setAllowMove] = useState(false);

  useEffect(() => {
    setTariffsOrdered(tariffs);
  }, [tariffs]);

  const onSortEnd = ({oldIndex, newIndex}) => {
    const orderedTariffs = arrayMove(tariffsOrdered, oldIndex, newIndex);
    let newOrderedTariffs = [];

    orderedTariffs.forEach((orderedTariff, i) => {
      newOrderedTariffs.push({...orderedTariff, position: i})
    });

    setTariffsOrdered(newOrderedTariffs);
  };

  const onAddTariff = () => {
    setOpen(!open);
  };

  const onDeleteTariff = (id) => {
    priceStore.deleteTariff(id);
  };

  const onSaveMovingTariff = () => {
    setAllowMove(false);
    priceStore.moveTariff(tariffsOrdered);
  };

  const onStartMovingTariffs = () => {
    setAllowMove(true)
  };

  useEffect(() => {
    priceStore.getTariffs();
  }, []);

  const renderAddTariffsCard = () => {
    if (priceStore.tariffList.length === 0) {
      return !loading && <Grid item className={classes.gridItem} onClick={onAddTariff}>
        <Paper className={classes.paper}>Добавить тариф</Paper>
      </Grid>
    }
  };

  const SortableItem = SortableElement(({value}) => {
    return (
      <Grid key={value._id} item className={classes.gridItem}>
        <IconButton
          onClick={() => onDeleteTariff(value._id)}
          className={classes.trash}>
          <DeleteIcon className='trash-icon'/>
        </IconButton>
        <NavLink
          to={`/content/tariff-page/${value._id}`}
          className={classes.link}>
          <Paper className={classes.paper}>
            {value.name}
          </Paper>
        </NavLink>
      </Grid>
    )
  });

  const SortableList = SortableContainer(({items}) => {
    return <Grid
      container
      className={classnames(classes.gridContainer, {'moving': allowMove})}
      spacing={2}>
      {renderAddTariffsCard()}
      {items.map((value, index) => (
        <SortableItem disabled={!allowMove} key={value._id} index={index} value={value}/>
      ))}
    </Grid>
  });

  const renderOrderingButtons = () => {
    if (!allowMove) {
      return <Button
        disabled={false}
        onClick={onStartMovingTariffs}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<FlipToFrontIcon/>}
      >Упорядочить</Button>
    } else {
      return <Button
        disabled={false}
        onClick={onSaveMovingTariff}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<SaveIcon/>}
      >Сохранить упорядочивание</Button>
    }
  };

  return (
    <StyledPricePage>
      <Typography variant="h4" component="h4" gutterBottom>Тарифы</Typography>

      {loading
        ? <CircularProgress className={classes.progress}/>
        : <>
          <SortableList
            axis={'xy'}
            items={tariffsOrdered}
            onSortEnd={onSortEnd}/>

          {priceStore.tariffList.length !== 0 && <Button
            onClick={onAddTariff}
            type='submit'
            variant="contained"
            color="primary"
            size="medium"
            className={classes.button}
            startIcon={<AddIcon/>}
          >Добавить тариф</Button>}

          {priceStore.tariffList.length !== 0 && renderOrderingButtons()}
        </>}

      <div>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={onAddTariff}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <Paper className={classes.modalPaper}>
              <CreateTariffForm/>
            </Paper>
          </Fade>
        </Modal>
      </div>
    </StyledPricePage>
  )
});