import React from 'react';
import {observer} from 'mobx-react';
import {StyledTariffPage} from "./styled";
import {UpdateTariffForm} from "../../../components/forms/UpdateTariffForm/UpdateTariffForm";

export const TariffPage = observer(props => {
  const {match} = props;
  const {params} = match;

  return <StyledTariffPage>
    <UpdateTariffForm id={params.id}/>
  </StyledTariffPage>
});