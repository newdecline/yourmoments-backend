import React from 'react';
import {StyledTrainingPage} from "./styled";
import {observer} from 'mobx-react';
import {PageData} from "./PageData/PageData";
import {makeStyles} from '@material-ui/core/styles';
import {Lessons} from "./Lessons/Lessons";

import {
  Divider,
  Typography,
} from "@material-ui/core";

import {blueGrey} from '@material-ui/core/colors';

const useStyles = makeStyles(() => ({
  divider: {
    margin: '30px 0',
    height: '5px',
    backgroundColor: blueGrey[900]
  },
}));

export const TrainingPage = observer(() => {
  const classes = useStyles();

  return <StyledTrainingPage>
    <Typography variant="h4" component="h4" gutterBottom>Редактирование страницы "Обучение"</Typography>

    <PageData/>
    <Divider className={classes.divider}/>

    <Typography variant="h4" component="h4" gutterBottom>Модули</Typography>

    <Lessons/>

  </StyledTrainingPage>
});