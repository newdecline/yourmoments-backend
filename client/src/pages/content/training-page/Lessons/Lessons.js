import React, {useEffect, useState} from 'react';
import {observer} from 'mobx-react';
import {blueGrey} from '@material-ui/core/colors';
import {makeStyles} from "@material-ui/core/styles";
import {NavLink} from "react-router-dom";
import lessonsStore from "../../../../mobx/LessonsStore";
import {SortableContainer, SortableElement} from "react-sortable-hoc";
import classnames from "classnames";
import arrayMove from "array-move";
import {CreateLessonForm} from "../../../../components/forms/CreateLessonForm/CreateLessonForm";

import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from '@material-ui/icons/Save';
import AddIcon from '@material-ui/icons/Add';
import FlipToFrontIcon from '@material-ui/icons/FlipToFront';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import {
  Button,
  Grid,
  IconButton,
  Paper,
  CircularProgress,
  Modal,
  Backdrop,
  Fade,
  Checkbox,
} from "@material-ui/core";

export const Lessons = observer(() => {
  const classes = useStyles();

  const loadingLessons = lessonsStore.loadingLessons;
  const updatingLessons = lessonsStore.updatingLessons;
  const lessons = lessonsStore.lessonsData;

  const [open, setOpen] = useState(false);

  const [lessonsOrdered, setLessonsOrdered] = useState(lessons || []);

  const [allowMove, setAllowMove] = useState(false);

  const onHideLesson = (e, _id) => {
    lessonsStore.updateHideLesson({
      _id,
      hide: e.target.checked
    })
  };

  const onSortEnd = ({oldIndex, newIndex}) => {
    const orderedLessons = arrayMove(lessonsOrdered, oldIndex, newIndex);
    let newOrderedLessons = [];

    orderedLessons.forEach((orderedTariff, i) => {
      newOrderedLessons.push({...orderedTariff, position: i})
    });

    setLessonsOrdered(newOrderedLessons);
  };

  const onAddLesson = () => {
    setOpen(!open);
  };

  const onDeleteLesson = (id) => {
    lessonsStore.deleteLessons({_id: id});
  };

  const onSaveMovingLesson = () => {
    setAllowMove(false);
    lessonsStore.moveLesson(lessonsOrdered);
  };

  const onStartMovingLesson = () => {
    setAllowMove(true);
  };

  const renderAddLessonCard = () => {
    if (lessons.length === 0) {
      return !loadingLessons && <Grid item className={classes.gridItem} onClick={onAddLesson}>
        <Paper className={classes.paper}>Добавить урок</Paper>
      </Grid>
    }
  };

  const renderOrderingButtons = () => {
    if (!allowMove) {
      return <Button
        disabled={updatingLessons}
        onClick={onStartMovingLesson}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<FlipToFrontIcon/>}
      >Упорядочить</Button>
    } else {
      return <Button
        disabled={updatingLessons}
        onClick={onSaveMovingLesson}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<SaveIcon/>}
      >Сохранить упорядочивание</Button>
    }
  };

  const SortableItem = SortableElement(({value}) => {
    return <Grid key={value._id} item className={classes.gridItem}>
      <Checkbox
        disabled={updatingLessons}
        checkedIcon={<VisibilityIcon className='visibility-icon'/>}
        icon={<VisibilityOffIcon className='visibility-icon'/>}
        className={classes.visibility}
        checked={value.hide}
        onChange={(e) => onHideLesson(e, value._id)}/>

      <IconButton
        disabled={updatingLessons}
        onClick={() => onDeleteLesson(value._id)}
        className={classes.trash}>
        <DeleteIcon className='trash-icon'/>
      </IconButton>

      <NavLink
        to={`/content/lesson-update-page/${value._id}`}
        className={classes.link}>
        <Paper className={classes.paper}>
          {value.name}
        </Paper>
      </NavLink>
    </Grid>
  });

  const SortableList = SortableContainer(({items}) => {
    return <>
      <Grid
        container
        className={classnames(classes.gridContainer, {'moving': allowMove})}
        spacing={2}>
        {renderAddLessonCard()}
        {items.map((value, index) => (
          <SortableItem disabled={!allowMove} key={value._id} index={index} value={value}/>
        ))}
      </Grid>

      {lessons.length !== 0 && <Button
        onClick={onAddLesson}
        disabled={updatingLessons}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<AddIcon/>}
      >Добавить урок</Button>}

      {lessons.length !== 0 && renderOrderingButtons()}
    </>
  });

  useEffect(() => {
    lessonsStore.getLessonsData();
  }, []);

  useEffect(() => {
    setLessonsOrdered(lessons);
  }, [lessons]);

  return <div className={classes.root}>
    {
      loadingLessons
        ? <CircularProgress className={classes.progress}/>
        : <SortableList
          axis={'xy'}
          items={lessonsOrdered}
          onSortEnd={onSortEnd}/>}

    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={onAddLesson}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Paper className={classes.modalPaper}>
            <CreateLessonForm/>
          </Paper>
        </Fade>
      </Modal>
    </div>
  </div>
});
const useStyles = makeStyles(theme => ({
  root: {
    '& .MuiTouchRipple-child-checked': {
      color: blueGrey[300],
    },
  },
  gridContainer: {
    flexGrow: 1,
    '&.moving': {
      backgroundColor: '#ccc'
    }
  },
  paper: {
    height: 140,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: 'background-color .3s',
    '&:hover': {
      backgroundColor: blueGrey[100],
      cursor: 'pointer'
    }
  },
  modalPaper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4),
    '&:focus': {
      outline: 'none'
    }
  },
  gridItem: {
    position: 'relative',
    width: '20%',
  },
  link: {
    display: 'block',
    fontSize: '20px',
    textDecoration: 'none',
    color: '#000',
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progress: {
    color: blueGrey[900]
  },
  trash: {
    position: 'absolute',
    right: 10,
    top: 10,
    zIndex: 1,
    backgroundColor: 'transparent',
    '& .trash-icon': {
      color: blueGrey[300],
    },
    '&:hover': {
      backgroundColor: 'transparent',
      '& .trash-icon': {
        color: blueGrey[700],
      }
    }
  },
  visibility: {
    position: 'absolute',
    left: 10,
    top: 10,
    zIndex: 1,
    backgroundColor: 'transparent',
    color: `${blueGrey[700]}!important`,
    '& .visibility-icon': {
      color: blueGrey[300],
    },
    '&:hover': {
      backgroundColor: 'transparent!important',
      '& .visibility-icon': {
        color: blueGrey[700],
      },
    }
  },
}));