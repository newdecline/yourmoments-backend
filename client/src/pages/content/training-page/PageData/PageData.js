import React, {useEffect, useState} from 'react';
import {observer} from 'mobx-react';
import {useForm} from 'react-hook-form';
import {EditorState, convertToRaw} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import trainingPageStore from '../../../../mobx/TrainingPageStore';
import {useNormalizeHTMLtoEditor} from "../../../../hooks/useNormalizeHTMLtoEditor";

import {
  Button,
  TextField,
  Typography,
  CircularProgress
} from "@material-ui/core";

import {makeStyles} from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';

import {blueGrey} from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
  root: {
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  divider: {
    margin: '30px 0'
  },
  editor: {
    border: '1px solid #f1f1f1'
  },
  editorWrapper: {
    width: '50%'
  },
  progress: {
    color: blueGrey[900]
  },
}));

export const PageData = observer(() => {
  const {register, handleSubmit, setValue} = useForm();
  const classes = useStyles();

  const loadingPageData = trainingPageStore.loadingPageData;
  const updatingPageData = trainingPageStore.updatingPageData;
  const pageData = trainingPageStore.pageData;

  const normalizedSubtitle1HTML = useNormalizeHTMLtoEditor(pageData.subtitle1HTML);
  const normalizedSubtitle2HTML = useNormalizeHTMLtoEditor(pageData.subtitle2HTML);

  const initialFormData = {
    title: pageData.title || '',
    note: pageData.note || '',
    costOfAllModules: pageData.costOfAllModules || '',
  };

  const [subtitle1HTML, setSubtitle1HTML] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!pageData.subtitle1HTML) {
      return
    }

    setSubtitle1HTML(normalizedSubtitle1HTML);
    setValue('subtitle1HTML', pageData.subtitle1HTML);
  }, [pageData.subtitle1HTML]);

  const [subtitle2HTML, setSubtitle2HTML] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!pageData.subtitle2HTML) {
      return
    }

    setSubtitle2HTML(normalizedSubtitle2HTML);
    setValue('subtitle2HTML', pageData.subtitle2HTML);
  }, [pageData.subtitle2HTML]);

  const onSubmit = async values => {
    trainingPageStore.updateTrainingPage({...values, _id: pageData._id});
  };

  const getResultsHTMLData = data => {
    switch (data.name) {
      case 'subtitle1HTML':
        return data;
      case 'subtitle2HTML':
        return data;
      default:
        return {}
    }
  };

  const onEditorStateChange = (editorState, data) => {
    const {name, setState} = getResultsHTMLData(data);

    setState(editorState);

    if (!editorState.getCurrentContent().hasText()) {
      setValue(name, '');
    } else {
      setValue(name, draftToHtml(convertToRaw(editorState.getCurrentContent())));
    }
  };

  useEffect(() => {
    register({name: 'subtitle1HTML'});
    register({name: 'subtitle2HTML'});

    trainingPageStore.getTrainingPage();
  }, []);

  if(loadingPageData) {
    return <CircularProgress className={classes.progress}/>
  }

  return <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
    <TextField
      id="title"
      label="Заголовок страницы"
      style={{margin: '16px 0'}}
      placeholder="Введите заголовок страницы"
      helperText="Это имя будет использоваться в качестве заголовка для страницы"
      fullWidth
      margin="normal"
      inputRef={register({required: true})}
      name='title'
      defaultValue={initialFormData.title}
      InputLabelProps={{shrink: true}}/>

    <Typography
      style={{fontWeight: 100}}
      variant="body1"
      component="p"
      gutterBottom>Подзаголвок-1</Typography>
    <Editor
      editorState={subtitle1HTML}
      editorClassName={classes.editor}
      wrapperClassName={classes.editorWrapper}
      onEditorStateChange={(editorState) => {
        onEditorStateChange(editorState, {
          name: 'subtitle1HTML',
          state: subtitle1HTML,
          setState: setSubtitle1HTML
        })
      }}/>

    <Typography
      style={{fontWeight: 100}}
      variant="body1"
      component="p"
      gutterBottom>Подзаголвок-2</Typography>
    <Editor
      editorState={subtitle2HTML}
      editorClassName={classes.editor}
      wrapperClassName={classes.editorWrapper}
      onEditorStateChange={(editorState) => {
        onEditorStateChange(editorState, {
          name: 'subtitle2HTML',
          state: subtitle2HTML,
          setState: setSubtitle2HTML
        })
      }}/>

    <TextField
      id="costOfAllModules"
      label="Стоимость"
      style={{margin: '16px 0'}}
      placeholder="Введите стоимость"
      helperText="Это имя будет использоваться в качестве стоимости за все модули"
      fullWidth
      margin="normal"
      inputRef={register()}
      name='costOfAllModules'
      defaultValue={initialFormData.costOfAllModules}
      InputLabelProps={{shrink: true}}/>

    <TextField
      id="note"
      label="Примечание"
      style={{margin: '16px 0'}}
      placeholder="Введите примечание"
      helperText="Это имя будет использоваться в качестве примечания"
      fullWidth
      margin="normal"
      inputRef={register()}
      name='note'
      defaultValue={initialFormData.note}
      InputLabelProps={{shrink: true}}/>

    <Button
      disabled={updatingPageData}
      type='submit'
      variant="contained"
      color="primary"
      size="medium"
      className={classes.button}
      startIcon={<SaveIcon/>}
    >{updatingPageData ? 'Изменение' : 'Сохранить'}</Button>
  </form>
});