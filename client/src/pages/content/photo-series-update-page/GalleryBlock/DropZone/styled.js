import styled from 'styled-components';
import {blueGrey} from '@material-ui/core/colors';

export const StyledDropZone = styled('div')`
  pointer-events: ${({disabled}) => disabled ? 'none' : 'all'};
  opacity: ${({disabled}) => disabled ? 0.5 : 1};
  .drop-area {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    background-color: ${({isDragActive}) => isDragActive
      ? `${blueGrey[300]}`
      : `${blueGrey[200]}`};
    border: 2px dashed ${blueGrey[100]};
    box-sizing: border-box;
    transition: background-color .3s;
    p {
      display: flex;
      align-items: center;
      padding: 50px 0;
      font-size: 16px;
      line-height: 120%;
    }
    svg {
      margin-left: 10px;
    }
  }
`;