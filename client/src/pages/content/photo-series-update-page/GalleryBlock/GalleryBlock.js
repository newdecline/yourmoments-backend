import React, {useState, useEffect} from 'react';
import {observer} from 'mobx-react';
import cls from 'classnames';
import {StyledGalleryBlock} from "./styled";
import {DropZone} from "./DropZone/DropZone";
import photoSeriesStore from "../../../../mobx/photoSeriesStore";
import photoSeriesGalleryStore from "../../../../mobx/photoSeriesGalleryStore";

import {
  makeStyles,
  Grid,
  Paper,
  IconButton, Button, CircularProgress, Typography, Divider
} from '@material-ui/core';

import {blueGrey} from "@material-ui/core/colors";

import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import SaveIcon from '@material-ui/icons/Save';

export const GalleryBlock = observer(props => {
  const {
    style,
    className,
  } = props;

  const classes = useStyles();

  const gallery = photoSeriesGalleryStore.photoSeriesGallery;
  const galleryId = photoSeriesStore.updatedPhotoSeries.galleryImages;

  const images = gallery.images || [];
  const loading = photoSeriesGalleryStore.loading;
  const updating = photoSeriesGalleryStore.updating;

  const [files, setFiles] = useState([]);

  const onUpdateFiles = newFiles => {
    setFiles(newFiles);
  };

  const onDeletePreSaveImage = lastModified => {
    setFiles(files.filter(file => file.lastModified !== lastModified));
  };

  const onAddImagesToPhotoSeriesGallery = () => {
    setFiles([]);
    photoSeriesGalleryStore.addImagesToPhotoSeriesGallery({files, galleryId});
  };

  const onDeleteImagesFromPhotoSeriesGallery = imageId => {
    photoSeriesGalleryStore.deleteImagesFromPhotoSeriesGallery(galleryId, imageId);
  };

  useEffect(() => {
    galleryId && photoSeriesGalleryStore.fetchPhotoSeriesGallery(galleryId);
  }, [galleryId]);

  const renderUploadedImages = () => {
    if (images.length === 0) {
      return <Typography variant="body2" component="p" gutterBottom>
        {updating ? 'Фотографии загружаются' : 'Пока нет загруженных фотографий'}
      </Typography>
    }

    return <Grid
      container
      justify="flex-start"
      spacing={3}
      className={cls(classes.gridContainer, classes.uploadedImagesContainer)}>
      {updating && updatingIndicator()}
      {images.map(image => (
        <Grid key={image.imageId} item className={classes.gridItem}>
          <Paper className={classes.paper}>
            <IconButton
              disabled={updating}
              onClick={() => onDeleteImagesFromPhotoSeriesGallery(image.imageId)}
              className={classes.trash}>
              <DeleteForeverIcon style={{color: '#fff'}}/>
            </IconButton>
            <img src={image.path} alt="#" className={classes.image}/>
          </Paper>
        </Grid>
      ))}
    </Grid>
  };

  const renderPreUploadedImages = () => {
    if (files.length !== 0) {
      return <>
        <Grid
          container
          justify="flex-start"
          spacing={3}
          className={classes.gridContainer}>
          {files.map(file => (
            <Grid key={file.lastModified} item className={classes.gridItem}>
              <Paper className={classes.paper}>
                <IconButton
                  onClick={() => onDeletePreSaveImage(file.lastModified)}
                  className={classes.trash}>
                  <DeleteForeverIcon style={{color: '#fff'}}/>
                </IconButton>
                <img src={file.preview} alt="#" className={classes.image}/>
              </Paper>
            </Grid>
          ))}
        </Grid>

        <div><Button
          disabled={files.length === 0}
          onClick={onAddImagesToPhotoSeriesGallery}
          type='submit'
          variant="contained"
          color="primary"
          size="medium"
          className={classes.button}
          startIcon={<SaveIcon/>}>Сохранить</Button></div>
      </>
    }

    return null
  };

  const updatingIndicator = () => {
    return <div className={classes.updatingIndicator}>
      <CircularProgress className={classes.progress}/>
    </div>
  };

  return <StyledGalleryBlock style={style} className={className}>
    {
      loading
        ? <CircularProgress className={classes.progress}/>
        : <>
          <Divider className={classes.divider}/>
          <Typography variant="h4" component="h4" gutterBottom>Фотогалерея</Typography>

          <DropZone onUpdateFiles={onUpdateFiles} disabled={loading}/>

          {renderPreUploadedImages()}

          <Divider className={classes.divider}/>
          <Typography variant="h4" component="h4" gutterBottom>Загруженные фотографии</Typography>

          {renderUploadedImages()}
        </>
    }
  </StyledGalleryBlock>
});

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  gridContainer: {
    position: 'relative',
    marginTop: 30,
    minHeight: '200px',
  },
  gridItem: {
    height: 200,
    width: '20%',
  },
  paper: {
    position: 'relative',
    height: '100%',
    width: '100%',
    overflow: 'hidden'
  },
  image: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    objectFit: 'cover'
  },
  trash: {
    position: 'absolute',
    right: 10,
    top: 10,
    zIndex: 1,
    backgroundColor: blueGrey[300],
    '&:hover': {
      backgroundColor: '#000',
    },
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(1),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  icon: {
    color: `${blueGrey[500]}!important`
  },
  progress: {
    color: blueGrey[900]
  },
  divider: {
    margin: '30px 0',
    height: '5px',
    backgroundColor: blueGrey[900]
  },
  updatingIndicator: {
    position: 'absolute',
    display: 'block',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    zIndex: 2,
    '& .MuiCircularProgress-indeterminate': {
      position: 'absolute',
      top: '50%',
      left: '50%',
      margin: '-20px -20px 0 0',
      color: '#fff'
    }
  }
}));