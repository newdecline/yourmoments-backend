import styled from 'styled-components';

export const StyledGalleryBlock = styled('div')`
  position: relative;
  margin: 40px 0;
  min-height: 20vh;
  .image-container {
    position: relative;
    display: inline-block;
    width: 120px;
    height: 70px;
    img {
      position: absolute;
      top: 0;
      left: 0;
      object-fit: contain;
      max-width: 100%;
      width: 100%;
      height: 100%;
    }
  }
  .overlay-loader {
    position: absolute;
    top: 12px;
    left: 12px;
    height: calc(100% - 23px);
    width: calc(100% - 24px);
    background-color: rgba(0, 0, 0, 0.5);
    border-radius: 5px;
    z-index: 2;
    svg {
      color: #fff;
    }
  }
`;