import React, {useState, useRef, useEffect} from 'react';
import {observer} from 'mobx-react';
import {Controller, useForm} from 'react-hook-form';
import {useQuery} from "../../../../hooks/useQuery.hook";
import photoSeriesStore from "../../../../mobx/photoSeriesStore";
import {Cropper} from "../../../../components/Cropper/Cropper";
import {DropZone} from "../../../../components/DropZone/DropZone";

import CropIcon from '@material-ui/icons/Crop';
import SaveIcon from '@material-ui/icons/Save';

import {blueGrey} from '@material-ui/core/colors';

import {
  Stepper,
  Step,
  StepButton,
  StepContent,
  Button,
  Typography,
  StepLabel,
  TextField,
  Select,
  FormControl,
  MenuItem,
  InputLabel, CircularProgress
} from '@material-ui/core';

import {makeStyles} from "@material-ui/core/styles";

export const SeriesBlock = observer(() => {
  const classes = useStyles();
  const query = useQuery();
  const slug = query.get('slug');

  const loading = photoSeriesStore.loading;
  const updating = photoSeriesStore.updating;

  const bannerCropperRef = useRef(null);
  const cardCropperRef = useRef(null);

  const [activeStep, setActiveStep] = useState(0);

  const [originalImage, setOriginalImage] = useState({});

  const [croppedBannerImage, setCroppedBannerImage] = useState({});
  useEffect(() => {
    if (!croppedBannerImage.path && !croppedBannerImage.file) {
      setValue('croppedBannerImage', undefined);
      setError('croppedBannerImage');
    } else {
      setValue('croppedBannerImage', croppedBannerImage);
      clearError('croppedBannerImage');
    }
  }, [croppedBannerImage.path, croppedBannerImage.file]);

  const [croppedCardImage, setCroppedCardImage] = useState({});
  useEffect(() => {
    if (!croppedCardImage.path && !croppedCardImage.file) {
      setValue('croppedCardImage', undefined);
      setError('croppedCardImage');
    } else {
      setValue('croppedCardImage', croppedCardImage);
      clearError('croppedCardImage');
    }
  }, [croppedCardImage.path, croppedCardImage.file]);

  const {
    register,
    handleSubmit,
    control,
    setValue,
    clearError,
    setError,
    errors,
    watch,
  } = useForm();

  const initialFormData = {
    name: photoSeriesStore.updatedPhotoSeries.name || '',
    urlVideo: photoSeriesStore.updatedPhotoSeries.urlVideo || '',
    category: photoSeriesStore.updatedPhotoSeries.category || '',
    originalImage: photoSeriesStore.updatedPhotoSeries.originalImage,
    croppedBannerImage: photoSeriesStore.updatedPhotoSeries.croppedBannerImage,
    croppedCardImage: photoSeriesStore.updatedPhotoSeries.croppedCardImage
  };

  useEffect(() => {
    const {originalImage, croppedBannerImage, croppedCardImage} = initialFormData;

    if (originalImage) {
      setOriginalImage({path: originalImage.path, imageId: originalImage.imageId});
      setValue('originalImage', {path: originalImage.path, imageId: originalImage.imageId});
      clearError('originalImage');
    }

    if (croppedBannerImage) {
      setCroppedBannerImage({path: croppedBannerImage.path, imageId: croppedBannerImage.imageId});
      setValue('croppedBannerImage', {path: croppedBannerImage.path, imageId: croppedBannerImage.imageId});
      clearError('croppedBannerImage');
    }

    if (croppedCardImage) {
      setCroppedCardImage({path: croppedCardImage.path, imageId: croppedCardImage.imageId});
      setValue('croppedCardImage', {path: croppedCardImage.path, imageId: croppedCardImage.imageId});
      clearError('croppedCardImage');
    }

  }, [
    initialFormData.originalImage,
    initialFormData.croppedBannerImage,
    initialFormData.croppedCardImage
  ]);

  const onCrop = cropperData => {
    const {ref, croppedImage, setCroppedImage} = cropperData;

    if (!ref.current.cropper.getCroppedCanvas()) {
      return false
    }

    ref.current.getCroppedCanvas().toBlob((blob) => {
      setCroppedImage({
        file: blob,
        cropperData: ref.current.getData(),
        imageId: croppedImage.imageId
      });
    }, 'image/jpeg');
  };

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleStep = step => {
    setActiveStep(step);
  };

  const onChangeOriginalImage = file => {
    setOriginalImage({file, imageId: originalImage.imageId});
    setValue('originalImage', {file, imageId: originalImage.imageId});
    clearError('originalImage');
  };

  const onDeleteOriginalImage = () => {
    setOriginalImage({imageId: originalImage.imageId});
    setValue('originalImage', undefined);
    setError('originalImage');

    setCroppedBannerImage({imageId: croppedBannerImage.imageId});
    setValue('croppedBannerImage', undefined);
    setError('croppedBannerImage');

    setCroppedCardImage({imageId: croppedCardImage.imageId});
    setValue('croppedCardImage', undefined);
    setError('croppedCardImage');
  };

  const watchName = watch('name', initialFormData.name);

  const steps = getSteps();

  const getCropperData = index => {
    switch (index) {
      case 1:
        return {
          ref: bannerCropperRef,
          croppedImage: croppedBannerImage,
          setCroppedImage: setCroppedBannerImage,
          aspectRatio: 19 / 4,
          originalImage
        };
      case 2:
        return {
          ref: cardCropperRef,
          croppedImage: croppedCardImage,
          setCroppedImage: setCroppedCardImage,
          aspectRatio: 6 / 4,
          originalImage
        };
      default:
        return {};
    }
  };

  const disabledStepsButtons = () => {
    return !originalImage.path && !originalImage.file
  };

  const disabledPhotoSeriesSaveButton = () => {
    if (errors.name
      || errors.originalImage
      || errors.croppedBannerImage
      || errors.croppedCardImage
      || watchName.trim() === ''
      || updating) {
      return true
    }
  };

  useEffect(() => {
    photoSeriesStore.fetchPhotoSeries({slug});

    register({name: 'originalImage'}, {required: true});
    register({name: 'croppedBannerImage'}, {required: true});
    register({name: 'croppedCardImage'}, {required: true});
  }, []);

  const onSubmit = async dataValues => {
    photoSeriesStore.updatePhotoSeries(dataValues, slug);
  };

  const renderInnerNextButton = (index) => {
    switch (index) {
      case 0:
        return 'Перейти для кадрирования';
      case 1:
        return <span className={classes.innerNextButton}>Следующий шаг</span>;
      case 2:
        return <span className={classes.innerNextButton}>Следующий шаг</span>;
      default:
        return false;
    }
  };

  return <div className={classes.root}>
    {
      loading
        ? <CircularProgress className={classes.progress}/>
        : <>
          <Typography variant="h4" component="h4" gutterBottom>
            Редактирование фотосерии: {initialFormData.name}
          </Typography>

          <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
            <Stepper className={classes.stepper} nonLinear activeStep={activeStep} orientation={"vertical"}>
              {steps.map((label, index) => {
                return (
                  <Step key={label}>
                    <StepButton disabled={disabledStepsButtons()} onClick={() => handleStep(index)}>
                      <StepLabel StepIconProps={{classes: {root: classes.icon}}}>{label}</StepLabel>
                    </StepButton>

                    <StepContent>
                      <Typography
                        variant="body2" component="span" gutterBottom
                      >{getStepContent(index)}</Typography>

                      {activeStep === 0 && (
                        <DropZone
                          onDeleteOriginalImage={onDeleteOriginalImage}
                          originalImage={originalImage}
                          onChangeOriginalImage={onChangeOriginalImage}/>)}

                      {activeStep !== 0 && (
                        <Cropper ref={getCropperData(index).ref} {...getCropperData(index)}/>)}

                      <div className={classes.actionsContainer}>
                        <Button
                          disabled={activeStep === 0}
                          onClick={handleBack}
                          className={classes.button}>Назад</Button>

                        {activeStep !== steps.length - 1 && (<Button
                          variant="contained"
                          color="primary"
                          onClick={handleNext}
                          disabled={disabledStepsButtons()}
                          className={classes.button}>{renderInnerNextButton(index)}</Button>)}

                        {activeStep !== 0 && (<Button
                          variant="contained"
                          color="primary"
                          onClick={() => onCrop({...getCropperData(index)})}
                          disabled={!originalImage}
                          className={classes.button}><span
                          className={classes.innerNextButton}>Обрезать <CropIcon/></span></Button>)}
                      </div>
                    </StepContent>
                  </Step>
                )
              })}
            </Stepper>
            <FormControl className={classes.formControl}>
              <InputLabel>Категория</InputLabel>
              <Controller
                as={
                  <Select>
                    <MenuItem value='paired'>Парные</MenuItem>
                    <MenuItem value='wedding'>Свадебные</MenuItem>
                    <MenuItem value="business-content">Контент для бизнеса</MenuItem>
                  </Select>
                }
                name="category"
                rules={register({required: true})}
                control={control}
                defaultValue={initialFormData.category}
              /></FormControl>

            <TextField
              label="Имя серии"
              style={{margin: '8px 0'}}
              placeholder="Введите имя серии"
              helperText="Это имя будет использоваться в качестве имени для серии"
              fullWidth
              margin="normal"
              name='name'
              defaultValue={initialFormData.name}
              inputRef={register({required: true})}/>

            <TextField
              label="Ссылка на ролик в youtube"
              style={{margin: '8px 0'}}
              placeholder="Вставьте полную ссылку с youtube"
              helperText="Эта ссылка будет использоваться в качестве ссылки для ролика на сайте"
              fullWidth
              margin="normal"
              name='urlVideo'
              defaultValue={initialFormData.urlVideo}
              inputRef={register()}/>

            <Button
              disabled={disabledPhotoSeriesSaveButton()}
              type='submit'
              variant="contained"
              color="primary"
              size="medium"
              className={classes.button}
              startIcon={<SaveIcon/>}>Сохранить</Button>
          </form>
        </>
    }
  </div>
});

const getSteps = () => [
  'Загрузите исходный файл',
  'Кадрируйте изображение под баннер',
  'Кадрируйте изображение под карточку'
];

const getStepContent = (step) => {
  switch (step) {
    case 0:
      return `Исходный файл используется только для кадрирования и не выводится на сайте`;
    case 1:
      return 'Это изобранжение будет показано на странице фотосерии';
    case 2:
      return `Это изобранжение будет показано в списках фотосерий`;
    default:
      return 'Неизвестный шаг';
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    width: '100%',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  stepper: {
    padding: 0
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  icon: {
    color: `${blueGrey[500]}!important`
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  innerNextButton: {
    display: 'flex'
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  formControl: {
    margin: '8px 0',
    minWidth: 120,
  },
  form: {
    position: 'relative',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  progress: {
    color: blueGrey[900]
  },
  overLayLoading: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
    zIndex: 2
  }
}));