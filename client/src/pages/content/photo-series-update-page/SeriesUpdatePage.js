import React from 'react';
import {observer} from 'mobx-react';
import {GalleryBlock} from "./GalleryBlock/GalleryBlock";
import {SeriesBlock} from "./SeriesBlock/SeriesBlock";

import {StyledSeriesUpdatePage} from "./styled";
import {makeStyles} from '@material-ui/core/styles';
import {blueGrey} from '@material-ui/core/colors';

export const SeriesUpdatePage = observer(() => {
  const classes = useStyles();

  return <StyledSeriesUpdatePage>
    <div className={classes.root}>
      <SeriesBlock/>
      <GalleryBlock/>
    </div>
  </StyledSeriesUpdatePage>
});

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    width: '100%',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    margin: '-20px -20px 0 0',
    color: blueGrey[900]
  }
}));