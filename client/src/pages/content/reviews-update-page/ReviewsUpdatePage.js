import React, {useEffect, useState} from 'react';
import {observer} from "mobx-react";
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from 'react-hook-form';
import reviewsStore from "../../../mobx/reviewsStore";
import {useNormalizeHTMLtoEditor} from "../../../hooks/useNormalizeHTMLtoEditor";

import {blueGrey} from "@material-ui/core/colors";
import {Save} from '@material-ui/icons';

import {
  Button,
  CircularProgress, TextField,
  Typography
} from "@material-ui/core";
import {convertToRaw, EditorState} from "draft-js";
import draftToHtml from "draftjs-to-html";
import {Editor} from "react-draft-wysiwyg";
import {UpdateReviewsImage} from "./UpdateReviewsImage/UpdateReviewsImage";

export const ReviewsUpdatePage = observer(props => {
  const classes = useStyles();

  const {match} = props;
  const {id} = match.params;

  const {register, handleSubmit, setValue, errors} = useForm();

  const reviews = reviewsStore.reviews.length !== 0 ? reviewsStore.reviews[0] : {};
  const loading = reviewsStore.loading;
  const updating = reviewsStore.updating;

  const initialFormData = {
    name: reviews.name || '',
  };

  const normalizedDescriptionHTML = useNormalizeHTMLtoEditor(reviews.descriptionHTML);

  const [descriptionHTML, setDescriptionHTML] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!reviews.descriptionHTML) {
      return
    }

    setDescriptionHTML(normalizedDescriptionHTML);
    setValue('descriptionHTML', reviews.descriptionHTML);
  }, [reviews.descriptionHTML]);

  const getResultsHTMLData = data => {
    switch (data.name) {
      case 'descriptionHTML':
        return data;
      default:
        return {}
    }
  };

  const onEditorStateChange = (editorState, data) => {
    const {name, setState} = getResultsHTMLData(data);

    setState(editorState);

    if (!editorState.getCurrentContent().hasText()) {
      setValue(name, '');
    } else {
      setValue(name, draftToHtml(convertToRaw(editorState.getCurrentContent())));
    }
  };

  const onSubmit = async values => {
    reviewsStore.updateReviews({...values, _id: id});
  };

  const getErrors = () => {
    return {
      name: errors.hasOwnProperty('name'),
    };
  };

  const isDisabledButtonSave = () => {
    return getErrors().name
  };

  useEffect(() => {
    reviewsStore.getReviews({_id: id});

    register({name: 'descriptionHTML'});
  }, [id]);

  return <div className={classes.root}>
    {
      loading
        ? <CircularProgress className={classes.progress}/>
        : <>
          <Typography variant="h4" component="h4" gutterBottom>Редактирование отзыва: {reviews.name}</Typography>

          <form onSubmit={handleSubmit(onSubmit)}>
            <TextField
              error={getErrors().name}
              label="Имя автора отзыва"
              style={{margin: '16px 0'}}
              placeholder="Введите имя автора отзыва"
              helperText="Это имя будет использоваться в качестве имени автора отзыва"
              fullWidth
              margin="normal"
              inputRef={register({required: true})}
              name='name'
              defaultValue={initialFormData.name}
              InputLabelProps={{
                shrink: true,
              }}/>

            <Typography
              style={{fontWeight: 100}}
              variant="body1"
              component="p"
              gutterBottom>Введите текст отзыва</Typography>
            <Editor
              editorState={descriptionHTML}
              editorClassName={classes.editor}
              wrapperClassName={classes.editorWrapper}
              onEditorStateChange={(editorState) => {
                onEditorStateChange(editorState, {
                  name: 'descriptionHTML',
                  state: descriptionHTML,
                  setState: setDescriptionHTML
                })
              }}/>

            <Button
              disabled={updating || isDisabledButtonSave()}
              type='submit'
              variant="contained"
              color="primary"
              size="medium"
              className={classes.button}
              startIcon={<Save/>}
            >Сохранить</Button>
          </form>

          <UpdateReviewsImage reviewsId={id}/>
        </>
    }
  </div>
});

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  progress: {
    color: blueGrey[900]
  },
  button: {
    marginTop: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  editor: {
    border: '1px solid #f1f1f1'
  },
  editorWrapper: {
    width: '50%'
  }
}));