import React, {useEffect, useState} from 'react';
import {observer} from 'mobx-react';
import {useForm} from 'react-hook-form';
import {EditorState, convertToRaw} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import {useNormalizeHTMLtoEditor} from "../../../hooks/useNormalizeHTMLtoEditor";
import lessonsStore from '../../../mobx/LessonsStore';
import {makeStyles} from '@material-ui/core/styles';

import {blueGrey} from '@material-ui/core/colors';

import {
  Button,
  Typography,
  CircularProgress,
  TextField
} from "@material-ui/core";

import SaveIcon from '@material-ui/icons/Save';

export const LessonUpdatePage = observer(props => {
  const {match} = props;
  const {params} = match;
  const classes = useStyles();
  const {register, handleSubmit, setValue} = useForm();

  const lesson = lessonsStore.lessonData;
  const loading = lessonsStore.loadingLessons;
  const updating = lessonsStore.updatingLessons;

  const initialFormData = {
    name: lesson.name || '',
    title: lesson.title || '',
    subtitle: lesson.subtitle || '',
  };

  const normalizedDescriptionHTML = useNormalizeHTMLtoEditor(lesson.descriptionHTML);

  const [descriptionHTML, setDescriptionHTML] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!lesson.descriptionHTML) {
      return
    }

    setDescriptionHTML(normalizedDescriptionHTML);
    setValue('descriptionHTML', lesson.descriptionHTML);
  }, [lesson.descriptionHTML]);

  const getResultsHTMLData = data => {
    switch (data.name) {
      case 'descriptionHTML':
        return data;
      default:
        return {}
    }
  };

  const onEditorStateChange = (editorState, data) => {
    const {name, setState} = getResultsHTMLData(data);

    setState(editorState);

    if (!editorState.getCurrentContent().hasText()) {
      setValue(name, '');
    } else {
      setValue(name, draftToHtml(convertToRaw(editorState.getCurrentContent())));
    }
  };

  const onSubmit = async values => {
    lessonsStore.updateLesson({
      ...values,
      _id: params.id
    })
  };

  useEffect(() => {
    register({name: 'descriptionHTML'});

    lessonsStore.getLessonData({_id: params.id});
  }, []);

  return <div className={classes.root}>
    <Typography variant="h4" component="h4" gutterBottom>Редактирование модуля</Typography>

    {loading
      ? <CircularProgress className={classes.progress}/>
      : <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
        <TextField
          id="standard-full-width"
          label="Название модуля"
          style={{margin: '16px 0'}}
          placeholder="Введите название модуля"
          helperText="Это имя будет использоваться в качестве названия модуля"
          fullWidth
          margin="normal"
          inputRef={register({required: true})}
          name='name'
          defaultValue={initialFormData.name}
          InputLabelProps={{shrink: true}}/>

        <TextField
          id="standard-full-width"
          label="Заголовок модуля"
          style={{margin: '16px 0'}}
          placeholder="Введите заголовок модуля"
          helperText="Это имя будет использоваться в качестве заголовка модуля"
          fullWidth
          margin="normal"
          inputRef={register()}
          name='title'
          defaultValue={initialFormData.title}
          InputLabelProps={{shrink: true}}/>

        <TextField
          id="standard-full-width"
          label="Подзаголовок модуля"
          style={{margin: '16px 0'}}
          placeholder="Введите подзаголовок модуля"
          helperText="Это имя будет использоваться в качестве подзаголовка модуля"
          fullWidth
          margin="normal"
          inputRef={register()}
          name='subtitle'
          defaultValue={initialFormData.subtitle}
          InputLabelProps={{shrink: true}}/>

        <Editor
          editorState={descriptionHTML}
          editorClassName={classes.editor}
          wrapperClassName={classes.editorWrapper}
          onEditorStateChange={(editorState) => {
            onEditorStateChange(editorState, {
              name: 'descriptionHTML',
              state: descriptionHTML,
              setState: setDescriptionHTML
            })
          }}/>

        <Button
          disabled={updating}
          type='submit'
          variant="contained"
          color="primary"
          size="medium"
          className={classes.button}
          startIcon={<SaveIcon/>}
        >{updating ? 'Изменение' : 'Сохранить'}</Button>
      </form>}

  </div>
});

const useStyles = makeStyles(theme => ({
  root: {
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  divider: {
    margin: '30px 0',
    height: '5px',
    backgroundColor: blueGrey[900]
  },
  progress: {
    color: blueGrey[900]
  },
  editor: {
    border: '1px solid #f1f1f1'
  },
  editorWrapper: {
    width: '50%'
  },
  form: {}
}));