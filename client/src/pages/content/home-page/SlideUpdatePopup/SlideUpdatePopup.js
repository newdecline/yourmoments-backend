import React, {useEffect, useState, useRef} from "react";
import {observer} from "mobx-react";
import {useForm} from 'react-hook-form';
import {DropZone} from "../../../../components/DropZone/DropZone";
import {Cropper} from "../../../../components/Cropper/Cropper";
import homePageSliderStore from "../../../../mobx/homePageSliderStore";

import {
  Button,
  Divider,
  Step,
  StepButton,
  StepContent,
  StepLabel,
  Stepper,
  Typography
} from "@material-ui/core";

import {makeStyles} from "@material-ui/core/styles";
import {blueGrey} from "@material-ui/core/colors";

import {Save, Crop} from '@material-ui/icons';

export const SlideUpdatePopup = observer(() => {
  const classes = useStyles();

  const slide = {};
  const updating = homePageSliderStore.updating;

  const cropperRef = useRef(null);

  const {
    register,
    handleSubmit,
    setValue,
    clearError,
    setError,
    errors,
  } = useForm();

  const [activeStep, setActiveStep] = useState(0);

  const [originalImage, setOriginalImage] = useState({});

  const [croppedImage, setCroppedImage] = useState({});
  useEffect(() => {
    if (!croppedImage.path && !croppedImage.file) {
      setValue('croppedImage', undefined);
      setError('croppedImage');
    } else {
      setValue('croppedImage', croppedImage);
      clearError('croppedImage');
    }
  }, [croppedImage.path, croppedImage.file]);

  useEffect(() => {
    register({name: 'originalImage'}, {required: true});
    register({name: 'croppedImage'}, {required: true});
  }, []);

  useEffect(() => {
    const {originalImage, croppedImage} = slide;

    if (originalImage) {
      setOriginalImage({path: originalImage.path, imageId: originalImage.imageId});
      setValue('originalImage', {path: originalImage.path, imageId: originalImage.imageId});
      clearError('originalImage');
    }

    if (croppedImage) {
      setCroppedImage({path: croppedImage.path, imageId: croppedImage.imageId});
      setValue('croppedImage', {path: croppedImage.path, imageId: croppedImage.imageId});
      clearError('croppedImage');
    }
  }, [slide]);

  const onSubmit = values => {
    homePageSliderStore.updateSlide(values);
  };

  const onCrop = cropperData => {
    const {ref, croppedImage, setCroppedImage} = cropperData;

    if (!ref.current.cropper.getCroppedCanvas()) {
      return false
    }

    ref.current.getCroppedCanvas().toBlob((blob) => {
      setCroppedImage({
        file: blob,
        cropperData: ref.current.getData(),
        imageId: croppedImage.imageId
      });
    }, 'image/jpeg');
  };

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleStep = step => {
    setActiveStep(step);
  };

  const onChangeOriginalImage = file => {
    setOriginalImage({file, imageId: originalImage.imageId});
    setValue('originalImage', {file, imageId: originalImage.imageId});
    clearError('originalImage');
  };

  const onDeleteOriginalImage = () => {
    setOriginalImage({imageId: originalImage.imageId});
    setValue('originalImage', undefined);
    setError('originalImage');

    setCroppedImage({imageId: croppedImage.imageId});
    setValue('croppedImage', undefined);
    setError('croppedImage');
  };

  const getErrors = () => {
    return {
      originalImage: errors.hasOwnProperty('originalImage'),
      croppedImage: errors.hasOwnProperty('croppedImage'),
    };
  };

  const isDisabledButtonSave = () => {
    return getErrors().originalImage || getErrors().croppedImage
  };

  const steps = getSteps();

  const getCropperData = index => {
    switch (index) {
      case 1:
        return {
          ref: cropperRef,
          croppedImage: croppedImage,
          setCroppedImage: setCroppedImage,
          aspectRatio: 2.5,
          originalImage
        };
      default:
        return {};
    }
  };

  const disabledStepsButtons = () => {
    return !originalImage.path && !originalImage.file
  };

  const renderInnerNextButton = index => {
    switch (index) {
      case 0:
        return 'Перейти для кадрирования';
      default:
        return '';
    }
  };

  return <div className={classes.root}>
    <Typography variant="body1" component="h6" gutterBottom>Добавить слайд в баннер</Typography>
    <Divider style={{margin: '0 0 10px 0'}}/>

    <form onSubmit={handleSubmit(onSubmit)} className={classes.form}>
      <Stepper className={classes.stepper} nonLinear activeStep={activeStep} orientation={"vertical"}>
        {steps.map((label, index) => {
          return (
            <Step key={label}>
              <StepButton disabled={disabledStepsButtons()} onClick={() => handleStep(index)}>
                <StepLabel StepIconProps={{classes: {root: classes.icon}}}>{label}</StepLabel>
              </StepButton>

              <StepContent>
                <Typography>{getStepContent(index)}</Typography>

                {activeStep === 0 && (
                  <DropZone
                    onDeleteOriginalImage={onDeleteOriginalImage}
                    originalImage={originalImage}
                    onChangeOriginalImage={onChangeOriginalImage}/>)}

                {activeStep !== 0 && (
                  <Cropper ref={getCropperData(index).ref} {...getCropperData(index)}/>)}

                <div className={classes.actionsContainer}>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.stepButton}>Назад</Button>

                  {activeStep !== steps.length - 1 && (<Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    disabled={disabledStepsButtons()}
                    className={classes.stepButton}>{renderInnerNextButton(index)}</Button>)}

                  {activeStep !== 0 && (<Button
                    variant="contained"
                    color="primary"
                    onClick={() => onCrop({...getCropperData(index)})}
                    disabled={!originalImage}
                    className={classes.stepButton}><span
                    className={classes.innerNextButton}>Обрезать <Crop/></span></Button>)}
                </div>
              </StepContent>
            </Step>
          )
        })}
      </Stepper>

      <Button
        disabled={updating || isDisabledButtonSave()}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<Save/>}
      >{updating ? 'Подождите, сохранение...' : 'Сохранить'}</Button>
    </form>
  </div>
});

const getSteps = () => [
  'Загрузите исходный файл',
  'Кадрируйте изображение'
];

const getStepContent = (step) => {
  switch (step) {
    case 0:
      return `Исходный файл используется только для кадрирования и не выводится на сайте`;
    case 1:
      return 'Это изобранжение будет показано в баннере на главной странице';
    default:
      return 'Неизвестный шаг';
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    width: '1300px',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  progress: {
    color: blueGrey[900]
  },
  button: {
    marginTop: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  stepButton: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  divider: {
    margin: '30px 0',
    height: '5px',
    backgroundColor: blueGrey[900]
  },
  icon: {
    color: `${blueGrey[500]}!important`
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  innerNextButton: {
    display: 'flex'
  },
  formControl: {
    margin: '8px 0',
    minWidth: 120,
  },
  form: {
    position: 'relative',
  },
}));