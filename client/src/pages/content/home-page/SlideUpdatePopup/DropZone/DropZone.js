import React, {useCallback, useState} from 'react';
import {useDropzone} from 'react-dropzone';
import {StyledDropZone} from "./styled";

import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';

const allowedMimeTypes = ['image/jpeg'];
const allowedMaximumFileSize = 1024 * 1024 * 15;

export const DropZone = props => {
  const {
    style,
    className,
    onUpdateFiles,
    disabled
  } = props;

  const [error, setError] = useState({});

  const onDrop = useCallback(acceptedFiles => {
    acceptedFiles.forEach(file => {
      if (file.size >= allowedMaximumFileSize) {
        return setError({
          type: 'fileSize',
          message: `Файл ${file.name} имеет слишком большой размер файла! Допускаются файлы размером не более ${allowedMaximumFileSize / 1024 / 1024} мБайт.`
        });
      } else if (!allowedMimeTypes.includes(file.type)) {
        return setError({type: 'mimeTypes', message: `Файл ${file.name} имеет недопустимый формат! Принимаются файлы типа "image/jpeg".`});
      } else {
        setError({});
        onUpdateFiles(acceptedFiles.map(file => Object.assign(file, {
          preview: URL.createObjectURL(file)
        })));
      }
    });
  }, []);

  const renderStartMessage = () => {
    if (error.type === 'mimeTypes') {
      return <p className='message'><span>{error.message}</span></p>
    } else if (error.type === 'fileSize') {
      return <p className='message'><span>{error.message}</span></p>
    } else {
      return disabled
        ? <p>Загрузка...</p>
        : <div className='start-help-text'>
          <span className='start-help-text__text'>Кликните сюда или перетащите файлы </span>
          <AddAPhotoIcon/>
        </div>
    }
  };

  const renderDropAre = isDragActive => {
    return <div className="drop-area">
      {
        isDragActive ?
          <div className='start-help-text'>Отпустите файлы <AddAPhotoIcon/></div> :
          renderStartMessage()
      }
    </div>
  };

  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});

  return <StyledDropZone
    style={style}
    className={className}
    isDragActive={isDragActive}
    disabled={disabled}>
    <div {...getRootProps()}>
      <input {...getInputProps()} />
      {renderDropAre(isDragActive)}
    </div>
  </StyledDropZone>
};