import styled from 'styled-components';
import {blueGrey} from '@material-ui/core/colors';

export const StyledDropZone = styled('div')`
  pointer-events: ${({disabled}) => disabled ? 'none' : 'all'};
  opacity: ${({disabled}) => disabled ? 0.5 : 1};
  .drop-area {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    background-color: ${({isDragActive}) => isDragActive
      ? `${blueGrey[300]}`
      : `${blueGrey[200]}`};
    border: 2px dashed ${blueGrey[100]};
    box-sizing: border-box;
    transition: background-color .3s;
    .start-help-text {
      display: flex;
      flex-direction: column;
      align-items: center;
      padding: 50px 0;
      font-size: 16px;
      line-height: 120%;
    }
    .start-help-text__text {
      display: block;
    }
    svg {
      margin: 10px 0 0 0;
    }
  }
`;