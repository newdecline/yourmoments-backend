import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Typography,
} from "@material-ui/core";
import {SlideList} from "./SlideList/SlideList";

export const HomePage = () => {
  const classes = useStyles();

  return <div className={classes.root}>
    <Typography variant="h4" component="h4" gutterBottom>Редактирование главной страницы</Typography>

    <SlideList/>
  </div>
};

const useStyles = makeStyles(theme => ({

}));