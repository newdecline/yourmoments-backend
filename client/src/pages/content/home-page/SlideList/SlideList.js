import React, {useState, useEffect} from "react";
import classnames from 'classnames';
import arrayMove from 'array-move';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';
import {observer} from 'mobx-react';
import {SlideUpdatePopup} from "../SlideUpdatePopup/SlideUpdatePopup";

import {
  Typography,
  Button,
  Paper,
  Grid,
  Divider,
  Modal,
  Backdrop,
  Fade,
  CircularProgress,
  IconButton
} from "@material-ui/core";

import {makeStyles} from '@material-ui/core/styles';
import {blueGrey} from '@material-ui/core/colors';

import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from '@material-ui/icons/Save';
import FlipToFrontIcon from '@material-ui/icons/FlipToFront';
import homePageSliderStore from "../../../../mobx/homePageSliderStore";

export const SlideList = observer(() => {
  const classes = useStyles();

  const [slidesOrdered, setSlidesOrdered] = useState([]);

  const [allowMove, setAllowMove] = useState(false);

  const loading = homePageSliderStore.loading;
  const updating = homePageSliderStore.updating;
  const openPopup = homePageSliderStore.openPopup;

  const slidesData = homePageSliderStore.slides;

  useEffect(() => {
    setSlidesOrdered(slidesData);
  }, [slidesData]);

  const onSortEnd = ({oldIndex, newIndex}) => {
    const orderedSlides = arrayMove(slidesOrdered, oldIndex, newIndex);
    let newOrderedSlides = [];

    orderedSlides.forEach((ordered, i) => {
      newOrderedSlides.push({...ordered, position: i})
    });

    setSlidesOrdered(newOrderedSlides);
  };

  const onAddSlide = () => {
    homePageSliderStore.setOpenPopup(!openPopup);
  };

  const onDeleteSlide = data => {
    homePageSliderStore.deleteSlide({
      slideId: data.slideId,
      originalImage: data.originalImage.imageId,
      croppedImage: data.croppedImage.imageId
    });
  };

  const onSaveMovingSlides = () => {
    setAllowMove(false);
    homePageSliderStore.moveSlide(slidesOrdered);
  };

  const onStartMovingSlides = () => {
    setAllowMove(true)
  };

  useEffect(() => {
    homePageSliderStore.getSlides()
  }, []);

  const renderAddSlideCard = () => {
    if (slidesData.length === 0) {
      return !loading && <Grid item className={classes.gridItem} onClick={onAddSlide}>
        <Paper className={classes.paper}>Добавить слайд</Paper>
      </Grid>
    }
  };

  const SortableItem = SortableElement(({value}) => {
    return <Grid key={value._id} item className={classes.gridItem}>
      <IconButton
        disabled={updating}
        onClick={() => onDeleteSlide(value)}
        className={classes.trash}>
        <DeleteIcon className='trash-icon'/>
      </IconButton>
        <Paper className={classnames(classes.paper, classes.slide, {'draggable': allowMove})}>
          <img className={classes.slideImage} src={value.croppedImage.path} alt={value.croppedImage.name}/>
        </Paper>
    </Grid>
  });

  const SortableList = SortableContainer(({items}) => {
    return <Grid
      container
      className={classnames(classes.gridContainer, {'moving': allowMove})}
      spacing={2}>
      {renderAddSlideCard()}
      {items.map((value, index) => (
        <SortableItem disabled={!allowMove} key={value.slideId} index={index} value={value}/>
      ))}
    </Grid>
  });

  const renderOrderingButtons = () => {
    if (!allowMove) {
      return <Button
        disabled={updating}
        onClick={onStartMovingSlides}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<FlipToFrontIcon/>}
      >Упорядочить</Button>
    } else {
      return <Button
        disabled={updating}
        onClick={onSaveMovingSlides}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<SaveIcon/>}
      >Сохранить упорядочивание</Button>
    }
  };

  return <div className={classes.root}>
    <Divider style={{margin: '0 0 20px 0'}}/>
    <Typography variant="h5" component="h4" gutterBottom>Баннер</Typography>

    {loading
      ? <CircularProgress className={classes.progress}/>
      : <>
        <SortableList
          axis={'xy'}
          items={slidesOrdered}
          onSortEnd={onSortEnd}/>

        {slidesData.length !== 0 && <Button
          disabled={updating}
          onClick={onAddSlide}
          type='submit'
          variant="contained"
          color="primary"
          size="medium"
          className={classes.button}
          startIcon={<AddIcon/>}
        >Добавить слайд</Button>}

        {slidesData.length !== 0 && renderOrderingButtons()}
      </>}

    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={openPopup}
        onClose={onAddSlide}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={openPopup}>
          <Paper className={classes.modalPaper}>
            <SlideUpdatePopup/>
          </Paper>
        </Fade>
      </Modal>
    </div>
  </div>
});

const useStyles = makeStyles(theme => ({
  gridContainer: {
    flexGrow: 1,
    '&.moving': {
      backgroundColor: '#ccc'
    }
  },
  paper: {
    height: 140,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: 'background-color .3s',
    '&:hover': {
      backgroundColor: blueGrey[100]
    }
  },
  modalPaper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4),
    '&:focus': {
      outline: 'none'
    }
  },
  gridItem: {
    position: 'relative',
    width: '20%',
    '& .draggable': {
      cursor: 'grab'
    }
  },
  link: {
    display: 'block',
    fontSize: '20px',
    textDecoration: 'none',
    color: '#000',
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    overflowY: 'auto'
  },
  progress: {
    color: blueGrey[900]
  },
  trash: {
    position: 'absolute',
    right: 10,
    top: 10,
    zIndex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    '& .trash-icon': {
      color: blueGrey[300],
    },
    '&:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.7)',
      '& .trash-icon': {
        color: blueGrey[700],
      }
    }
  },
  slide: {
    position: 'relative'
  },
  slideImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    objectFit: 'cover'
  },
}));