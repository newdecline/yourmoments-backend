import React, {useState, useEffect} from "react";
import {StyledListPhotoSeriesPage} from "./styled";
import {observer} from "mobx-react";
import {customHistory} from "../../../../customHistory";
import photoSeriesListStore from "../../../../mobx/photoSeriesListStore";
import {CreatePhotoSeriesForm} from "../../../../components/forms/CreatePhotoSeriesForm/CreatePhotoSeriesForm";

import blueGrey from '@material-ui/core/colors/blueGrey';

import {makeStyles} from '@material-ui/core/styles';

import {AddAPhoto, Edit, Delete} from '@material-ui/icons';

import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  IconButton,
  Modal,
  Backdrop,
  Fade,
  Typography, CircularProgress
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    minHeight: 'calc(100vh - 186px)',
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    '&:focus': {
      outline: 'none'
    }
  },
  button: {
    margin: '10px 0',
    color: '#000',
    '&:hover': {
      backgroundColor: blueGrey[400],
      color: '#000'
    }
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4),
    '&:focus': {
      outline: 'none'
    }
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    margin: '-20px -20px 0 0',
    color: blueGrey[900]
  }
}));

const columns = [
  {
    id: 'name',
    label: 'Название серии',
    minWidth: 170,
    align: 'left'
  }
];

export const PhotoSeriesListPage = observer(props => {
  const {match: {params}} = props;

  const classes = useStyles();

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleClickAddSeriesBtn = () => {
    setOpen(true);
  };

  const handleClickUpdatePhotoSeries = slug => {
    customHistory.push(`/content/photo-series-update?slug=${slug}`)
  };

  const handleClickDeletePhotoSeries = slug => {
    photoSeriesListStore.deletePhotoSeries(slug);
  };

  const rows = [...photoSeriesListStore.photoSeriesList];

  useEffect(() => {
    photoSeriesListStore.fetchPhotoSeriesByCategory({category: params.categoryName});
  }, [params.categoryName]);

  return (
    <StyledListPhotoSeriesPage>
      <Paper className={classes.root}>
        {photoSeriesListStore.loading
          ? <CircularProgress className={classes.progress}/>
          : <>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map(column => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{borderBottom: `1px solid ${blueGrey[900]}`}}
                      >
                        <Typography variant="h6">
                          Добавить фотосерию
                          <IconButton
                            onClick={handleClickAddSeriesBtn}
                            style={{
                              marginLeft: 10,
                              color: blueGrey[900]
                            }}
                            size={"medium"}
                            aria-label="add">
                            <AddAPhoto/>
                          </IconButton>
                        </Typography>
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>

                <TableBody>
                  <TableRow>
                    {columns.map(column => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{minWidth: column.minWidth, fontWeight: 700}}>
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                  {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                    return (
                      <TableRow hover role="checkbox" tabIndex={-1} key={row.slug}>
                        {columns.map(column => {
                          const value = row[column.id];

                          return (
                            <TableCell
                              key={row.slug}
                              align={column.align}
                              style={{
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'space-between'
                              }}>
                              {value}
                              <div>
                                <IconButton
                                  aria-label="add"
                                  size={'medium'}
                                  onClick={() => handleClickUpdatePhotoSeries(row.slug)}>
                                  <Edit/>
                                </IconButton>
                                <IconButton
                                  aria-label="add"
                                  size={'medium'}
                                  onClick={() => handleClickDeletePhotoSeries(row.slug)}>
                                  <Delete/>
                                </IconButton>
                              </div>
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </TableContainer>

            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </>}
      </Paper>

      <div>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <Paper className={classes.paper}>
              <CreatePhotoSeriesForm/>
            </Paper>
          </Fade>
        </Modal>
      </div>
    </StyledListPhotoSeriesPage>
  )
});