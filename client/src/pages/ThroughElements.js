import React, {useEffect} from 'react';
import {observer} from 'mobx-react';
import {useForm} from 'react-hook-form';
import throughElementsStore from '../mobx/throughElementsStore';

import {makeStyles} from "@material-ui/core/styles";
import {blueGrey} from "@material-ui/core/colors";

import {
  Button,
  Typography,
  CircularProgress,
  TextField,
  Divider
} from "@material-ui/core";

import SaveIcon from '@material-ui/icons/Save';


export const ThroughElements = observer(() => {
  const classes = useStyles();
  const {register, handleSubmit} = useForm();

  const loading = throughElementsStore.loading;
  const updating = throughElementsStore.updating;
  const throughElements = throughElementsStore.throughElements;

  const onSubmit = async values => {
    throughElementsStore.updateThroughElements(values);
  };

  useEffect(() => {
    throughElementsStore.getThroughElements();
  }, []);

  const initialFormData = {
    vkontakte: throughElements.vkontakte || '',
    instagram: throughElements.instagram || '',
    email: throughElements.email || '',
    phone1: throughElements.phone1 || '',
    phone2: throughElements.phone2 || '',
  };

  return <div className={classes.root}>
    <Typography variant="h4" component="h4" gutterBottom>Общие данные</Typography>

    {loading
      ? <CircularProgress className={classes.progress}/>
      : <>
        <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
          <Typography style={{margin: '20px 0 0 0'}} variant="body1" component="h6" gutterBottom>Социальные сети</Typography>
          <Divider/>

          <TextField
            style={{margin: '16px 0'}}
            placeholder="Введите ссылку vkontakte"
            helperText="Эта ссылка будет использоваться в качестве ссылки на Ваш vkontakte"
            fullWidth
            margin="normal"
            inputRef={register({required: true})}
            name='vkontakte'
            defaultValue={initialFormData.vkontakte}
            InputLabelProps={{shrink: true}}/>

          <TextField
            style={{margin: '16px 0'}}
            placeholder="Введите ссылку instagram"
            helperText="Эта ссылка будет использоваться в качестве ссылки на Ваш instagram"
            fullWidth
            margin="normal"
            inputRef={register({required: true})}
            name='instagram'
            defaultValue={initialFormData.instagram}
            InputLabelProps={{shrink: true}}/>

          <Typography style={{margin: '20px 0 0 0'}} variant="body1" component="h6" gutterBottom>Почта</Typography>
          <Divider/>

          <TextField
            style={{margin: '16px 0'}}
            placeholder="Введите адрес электронной почты"
            helperText="Будет использоваться в качестве почтового адреса"
            fullWidth
            margin="normal"
            inputRef={register({required: true})}
            name='email'
            defaultValue={initialFormData.email}
            InputLabelProps={{shrink: true}}/>

          <Typography style={{margin: '20px 0 0 0'}} variant="body1" component="h6" gutterBottom>Номера телефонов</Typography>
          <Divider/>

          <TextField
            style={{margin: '16px 0'}}
            label='Первый номер телефона'
            placeholder="Введите номер телефона"
            helperText="Будет использоваться в качестве первого и основного телефона"
            fullWidth
            margin="normal"
            inputRef={register({required: true})}
            name='phone1'
            defaultValue={initialFormData.phone1}
            InputLabelProps={{shrink: true}}/>

          <TextField
            style={{margin: '16px 0'}}
            label='Второй номер телефона'
            placeholder="Введите номер второго телефона"
            helperText="Будет использоваться в качестве второго телефона"
            fullWidth
            margin="normal"
            inputRef={register({required: true})}
            name='phone2'
            defaultValue={initialFormData.phone2}
            InputLabelProps={{shrink: true}}/>


          <Button
            disabled={updating}
            type='submit'
            variant="contained"
            color="primary"
            size="medium"
            className={classes.button}
            startIcon={<SaveIcon/>}
          >{updating ? 'Изменение' : 'Сохранить'}</Button>
        </form>
      </>}
  </div>
});

const useStyles = makeStyles((theme) => ({
  root: {
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  divider: {
    margin: '30px 0',
    height: '5px',
    backgroundColor: blueGrey[900]
  },
  progress: {
    color: blueGrey[900]
  },
  editor: {
    border: '1px solid #f1f1f1'
  },
  editorWrapper: {
    width: '50%'
  },
  form: {}
}));