import React from 'react';
import styled from "styled-components/macro";
import {darken} from "polished";

export const IndexPage = () => {
    const greetings = [
        {
            range: [0, 5],
            message: 'Доброй ночи zzz...',
        },
        {
            range: [5, 12],
            message: 'С добрым утром!',
        },
        {
            range: [12, 17],
            message: 'Добрый день!',
        },
        {
            range: [17, 24],
            message: 'Добрый вечер!',
        }
    ];

    const hour = (new Date()).getHours();
    const greeting = greetings.find(item => item.range[0] <= hour && hour < item.range[1]).message;

    return (
        <IndexPageStyled>
            <h2>{greeting}</h2>
            <p>Вы можете выбрать категорию администрирования выше</p>
        </IndexPageStyled>
    )
};

const IndexPageStyled = styled('div')`
    h2 {
        margin: 46px 0 20px 0;
        text-align: center;
        font-size: 56px;
        font-weight: 400;
    }
    p {
        text-align: center;
        font-size: 26px;
        font-weight: 300;
        color: ${darken(0.6, '#fff')};
    }
`;