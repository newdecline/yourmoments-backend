import React from 'react';
import {observer} from 'mobx-react';
import {useForm} from "react-hook-form";
import {authStore} from "../../mobx/authStore";
import {AuthPageStyled} from "./styled";

import {
  makeStyles,
  Button,
  TextField,
  CircularProgress
} from '@material-ui/core';

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {blueGrey} from "@material-ui/core/colors";

export const AuthPage = observer(() => {
  const loading = authStore.loading;

  const classes = useStyles();

  const {register, handleSubmit, errors} = useForm();

  const onSubmit = async dataValues => {
    authStore.login(dataValues);
  };

  return (
    <AuthPageStyled className={classes.root}>
      <div className="form-wrapper">
        <AccountCircleIcon className='form-wrapper__icon-top'/>
        <form onSubmit={handleSubmit(onSubmit)} className='form'>
          <TextField
            error={!!errors.email}
            helperText={!!errors.email ? 'Не верный формат почты' : ''}
            label="Введите e-mail"
            type='email'
            name="email"
            inputRef={register({required: true})}
          />
          <TextField
            error={!!errors.password}
            helperText={!!errors.password ? 'Не верный пароль или логин' : ''}
            label="Введите пароль"
            type="password"
            name="password"
            inputRef={register({required: true})}
          />
          <Button
            type='submit'
            style={{margin: '20px 0 0 0'}}
            variant="contained">{loading ? <CircularProgress size={21}/> : 'Войти'}</Button>
        </form>
      </div>
    </AuthPageStyled>
  )
});

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  button: {
    marginTop: theme.spacing(2),
    marginRight: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
}));