import styled from "styled-components";

export const AuthPageStyled = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
  background: #fff;
  .form-wrapper {
    position: relative;
    padding: 40px;
    box-sizing: border-box;
    border-radius: 8px;
    background-color: #d6d6d6;
  &__icon-top {
    position: absolute;
    top: -30px;
    left: 50%;
    width: 60px;
    height: auto;
    transform: translate(-50%, 0);
    }
  }
  .form {
    display: flex;
    flex-direction: column;
    width: 300px;
  }
`;