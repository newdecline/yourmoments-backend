import {createGlobalStyle} from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html, body {
    font-family: 'Roboto', sans-serif;
    min-height: 100vh;
  }
  
  button, input {
    font-family: 'Roboto', sans-serif;
    &:focus, 
    &:focus {
      outline: none;
    }
  }
  .my-masonry-grid {
    display: -webkit-box; 
    display: -ms-flexbox;
    display: flex;
    margin-top: 40px;
    margin-left: -24px;
    width: auto;
  }
  .my-masonry-grid_column {
    padding-left: 24px; 
    background-clip: padding-box;
  }
  
  .my-masonry-grid_column > div { 
    background: grey;
    margin-bottom: 30px;
  }
`;