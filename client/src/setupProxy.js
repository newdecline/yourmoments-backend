const proxy = require('http-proxy-middleware');

module.exports = function (app) {
  app.use(
    ['/api', '/uploads'],
    proxy({
      target: process.env.REACT_APP_PROXY_URL,
      changeOrigin: true,
      headers: {
        "Connection": "keep-alive"
      },
    })
  );
};