import {authStore} from "../mobx/authStore";
import {AlertMessage} from "../components/AlertMessage";
import React from "react";

const REQUEST_DEFAULT_OPTIONS = {
  method: 'get',
  contentType: 'application/json',
  body: {},
  query: {},
};

class HttpService {
  apiBase;

  constructor(apiBase) {
    this.apiBase = apiBase;
  }

  async makeRequest(route, options = {}) {
    const accessToken = localStorage.getItem("accessToken") || '';

    const {
      contentType,
      method,
      body,
      query,
    } = {...REQUEST_DEFAULT_OPTIONS, ...options};

    let fetchConfig = {
      headers: {
        'x-access-token': accessToken.replace(/"/g, "")
      },
      method,
    };

    if (contentType) {
      fetchConfig.headers['Content-Type'] = contentType;
    }

    if (method !== 'get' && method !== 'head') {
      fetchConfig.body = contentType === 'application/json' ? JSON.stringify(body) : body;
    }

    const urlObject = new URL(`${this.apiBase}${route}`, window.location.origin);

    for (const key in query) {
      urlObject.searchParams.append(key, query[key]);
    }

    const response = await fetch(urlObject, fetchConfig);

    let responseBody;
    try {
      responseBody = await response.json();
    } catch (e) {
      responseBody = null;
    }

    if (response.status === 401) {
      authStore.autoLogout();
    }

    if (response.status === 400 || response.status === 500 || response.status === 502 || response.status === 503) {
      window.reactAlert.error(<AlertMessage message='Произошла ошибка, напишите разработчикам'/>);
    }

    return {
      status: response.status,
      body: responseBody,
    };
  }

  async post(route, body, options = {}) {
    return await this.makeRequest(route, {...options, method: 'post', body});
  }

  async get(route, options = {}) {
    return await this.makeRequest(route, options);
  }

  async put(route, body, options = {}) {
    const result = await this.makeRequest(route, {...options, method: 'put', body});

    if (result.status === 200) {
      console.log('Данные сохранены');
    } else {
      console.log('Ошибка при сохранении данных');
    }

    return result;
  }

  async delete(route, body, options = {}) {
    const result = await this.makeRequest(route, {...options, method: 'delete', body});

    if (result.status === 200) {
      console.log('Данные сохранены');
    } else {
      console.log('Ошибка при сохранении данных');
    }

    return result;
  }
}

export default HttpService;