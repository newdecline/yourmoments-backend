import HttpService from "./HttpService";
import { objectToFormData } from 'object-to-formdata';

export const AUTH_ROUTE = '/auth';
export const PHOTO_SERIES_ROUTE = '/photo-series';
export const PHOTO_GALLERY_ROUTE = '/photo-gallery';
// export const PRICE_ROUTE = '/price';
export const TARIFF_ROUTE = '/price/tariff';
export const TRAINING_ROUTE = '/training-page';
export const REVIEWS_ROUTE = '/reviews-page';
export const HOMEPAGE_ROUTE = '/home-page';
export const THROUGH_ELEMENTS_ROUTE = '/through-elements-page';

class ApiService extends HttpService {
  async login(data) {
    return await this.post(`${AUTH_ROUTE}/login`, data);
  }

  async logout() {
    return await this.post(`${AUTH_ROUTE}/logout`, {});
  }

//--photo-series
  async createPhotoSeries(data) {
    const {category, name} = data;

    let formData = new FormData();

    formData.append('category', category);
    formData.append('name', name);

    return await this.post(`${PHOTO_SERIES_ROUTE}`, formData, {contentType: false});
  }

  async getPhotoSeries(query) {
    return await this.get(`${PHOTO_SERIES_ROUTE}`, {
      query: query
    });
  }

  async putPhotoSeries(data, slug) {
    const {
      category,
      name,
      urlVideo,
      originalImage,
      croppedBannerImage,
      croppedCardImage
    } = data;

    const formData = objectToFormData(
      {
        category,
        name,
        urlVideo,
        originalImage: originalImage.file || originalImage.path,
        originalImageId: originalImage.imageId || undefined,
        croppedBannerImage: {
          imageId: croppedBannerImage.imageId || undefined,
          cropperData: croppedBannerImage.cropperData || undefined
        },
        croppedCardImage: {
          imageId: croppedCardImage.imageId || undefined,
          cropperData: croppedCardImage.cropperData || undefined
        }
      }
    );


    return await this.put(`${PHOTO_SERIES_ROUTE}`, formData, {
      query: {slug},
      contentType: false
    });
  }

  async deletePhotoSeries(slug) {
    const formData = objectToFormData({slug});

    return await this.delete(`${PHOTO_SERIES_ROUTE}`, formData, {contentType: false});
  }

//--photo-series-gallery
  async fetchPhotoSeriesGallery(galleryId) {
    return await this.get(`${PHOTO_GALLERY_ROUTE}/${galleryId}`);
  }

  async addImagesToPhotoSeriesGallery({files, galleryId}) {
    let formData = objectToFormData({files});

    return await this.post(`${PHOTO_GALLERY_ROUTE}/${galleryId}`,
      formData,
      {
        contentType: false
      });
  }

  async deleteImagesFromPhotoSeriesGallery(galleryId, imageId) {
    return await this.delete(`${PHOTO_GALLERY_ROUTE}/${galleryId}/${imageId}`);
  }

//--tariff
  async createTariff(data) {
    const formData = objectToFormData(data);

    return await this.post(`${TARIFF_ROUTE}`, formData, {contentType: false});
  }

  async getTariffs(data = {}) {
    return await this.get(`${TARIFF_ROUTE}`,{
      query: data
    });
  }

  async updateTariff(data) {
    const formData = objectToFormData(data);

    return await this.put(`${TARIFF_ROUTE}`, formData, {contentType: false});
  }

  async deleteTariff(id) {
    return await this.delete(`${TARIFF_ROUTE}/${id}`);
  }

  async moveTariff(data) {
    return await this.put(`${TARIFF_ROUTE}/move`, data);
  }

//--training-page
  async getTrainingPage() {
    return await this.get(`${TRAINING_ROUTE}`);
  }

  async updateTrainingPage(data) {
    const formData = objectToFormData(data);

    return await this.put(`${TRAINING_ROUTE}`, formData, {contentType: false});
  }

//--lessons
  async getLessonsData(data = {}) {
    return await this.get(`${TRAINING_ROUTE}/lessons`, {
      query: data
    });
  }

  async createLessonsData(data) {
    const formData = objectToFormData(data);

    return await this.post(`${TRAINING_ROUTE}/lessons`, formData, {contentType: false});
  }

  async moveLesson(data) {
    return await this.put(`${TRAINING_ROUTE}/lessons/move`, data);
  }

  async updateLesson(data) {
    const formData = objectToFormData(data);

    return await this.put(`${TRAINING_ROUTE}/lessons`, formData, {contentType: false});
  }

  async updateHideLesson(data) {
    const formData = objectToFormData(data);

    return await this.put(`${TRAINING_ROUTE}/lessons/hide`, formData, {contentType: false});
  }

  async deleteLessons(data) {
    const formData = objectToFormData(data);

    return await this.delete(`${TRAINING_ROUTE}/lessons`, formData, {contentType: false});
  }

//--reviews
  async getReviews(data) {
    return await this.get(`${REVIEWS_ROUTE}`, {
      query: data || {}
    });
  }

  async createReviews(data) {
    return await this.post(`${REVIEWS_ROUTE}`, data);
  }

  async updateReviews(data) {
    return await this.put(`${REVIEWS_ROUTE}`, data);
  }

  async updateImageReviews(data) {
    const {
      originalImage,
      croppedImage,
    } = data;

    const formData = objectToFormData(
      {
        originalImage: originalImage.file || originalImage.path,
        originalImageId: originalImage.imageId || undefined,
        croppedImage: {
          imageId: croppedImage.imageId || undefined,
          cropperData: croppedImage.cropperData || undefined
        },
      }
    );

    return await this.put(`${REVIEWS_ROUTE}/image`, formData, {
      query: {_id: data._id},
      contentType: false
    });
  }

  async moveReviews(data) {
    return await this.put(`${REVIEWS_ROUTE}/move`, data);
  }

  async deleteReviews(data) {
    return await this.delete(`${REVIEWS_ROUTE}`, {}, {
      query: data
    });
  }

//--homePageSlider
  async getSlides(data) {
    return await this.get(`${HOMEPAGE_ROUTE}/banner`, {
      query: data || {}
    });
  }

  async updateSlide(data) {
    const {
      originalImage,
      croppedImage,
    } = data;

    const formData = objectToFormData(
      {
        originalImage: originalImage.file || originalImage.path,
        originalImageId: originalImage.imageId || undefined,
        croppedImage: {
          imageId: croppedImage.imageId || undefined,
          cropperData: croppedImage.cropperData || undefined
        },
      }
    );

    return await this.put(`${HOMEPAGE_ROUTE}/banner`, formData, {contentType: false});
  }

  async moveSlide(data) {
    return await this.put(`${HOMEPAGE_ROUTE}/banner/move-slide`, data);
  }

  async deleteSlide(data) {
    return await this.delete(`${HOMEPAGE_ROUTE}/banner`, {}, {
      query: data
    });
  }

  //--throughElements
  async getThroughElements() {
    return await this.get(`${THROUGH_ELEMENTS_ROUTE}`);
  }

  async updateThroughElements(data) {
    return await this.put(`${THROUGH_ELEMENTS_ROUTE}`, data);
  }
}

export default new ApiService('/api');