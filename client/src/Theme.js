import {ThemeProvider} from "styled-components";
import React from "react";

const theme = {};

export const Theme = ({children}) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);