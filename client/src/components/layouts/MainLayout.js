import React from "react";
import {observer} from "mobx-react";
import styled from "styled-components";
import {Switch, Route} from "react-router-dom";
import {Header} from "../Header";
import {NavBar} from "../NavBar";
import {AppDrawer} from "../../components/AppDrawer";

import {makeStyles} from "@material-ui/core/styles";


const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  content: {
    flexGrow: 1,
    marginTop: 106,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
}));

export const MainLayout = observer(props => {
  const {content: Content, drawer, path: layoutPath} = props;

  const drawerRoutes = drawer && drawer.reduce((a, b) => [
    ...a,
    ...[
      {
        ...b,
        path: layoutPath + b.path,
        as: layoutPath + b.as
      }
      ],
    ...(b.children ? b.children.map(child => ({
      ...child,
      nested: true,
      path: layoutPath + b.path + child.path,
      as: layoutPath + b.as + child.as,
    })) : [])
  ], []);

  const classes = useStyles();

  return <StyledMainLayout>
    <div className={classes.root}>
      <Header/>
      <NavBar/>
      {drawer && <AppDrawer items={drawerRoutes}/>}

      <main className={classes.content}>
        {Content && !drawer && <Content/>}
        {drawer &&
        <Switch>
          {Content && <Route key="root" path={layoutPath} component={Content} exact={true}/>}
          {drawerRoutes.map((props, i) => {
            return <Route key={i} path={props.path} component={props.page} exact={props.exact}/>})}
        </Switch>
        }
      </main>
    </div>
  </StyledMainLayout>
});

const StyledMainLayout = styled('div')`

`;