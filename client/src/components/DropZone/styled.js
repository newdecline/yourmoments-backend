import styled from 'styled-components';
import {blueGrey} from '@material-ui/core/colors';

export const StyledDropZone = styled('div')`
  border: ${({originImage}) => originImage
  ? `0 dashed transparent`
  : `2px dashed ${blueGrey[100]}`};
  background-color: ${({isDragActive}) => isDragActive
  ? blueGrey[300]
  : blueGrey[200]};
  box-sizing: border-box;
  transition: background-color .3s;
  .message {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 50px 0;
    margin: 0;
    text-align: center;
    box-sizing: border-box;
    span {
      margin-right: 10px;
      font-size: 16px;
      line-height: 24px;
    }
  }
  .loaded-original-image {
    position: relative;
    height: 400px;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    button {
      position: absolute;
      top: 15px;
      left: 15px;
    }
    img {
      max-width: 100%;
      max-height: 100%;
    }
  }
`;