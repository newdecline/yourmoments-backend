import React, {useState, useEffect, useCallback} from 'react';
import {useDropzone} from 'react-dropzone';
import {StyledDropZone} from "./styled";

import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import DeleteIcon from '@material-ui/icons/Delete';

import IconButton from "@material-ui/core/IconButton";
import {getBase64} from "../../utilities/getBase64";

const allowedMimeTypes = ['image/jpeg'];
const allowedMaximumFileSize = 1024 * 1024 * 15;

export const DropZone = props => {
  const {
    onChangeOriginalImage,
    originalImage,
    onDeleteOriginalImage
  } = props;

  const [preview, setPreview] = useState(undefined);
  useEffect(() => {
    if (originalImage) {
      if (originalImage.hasOwnProperty('path')) {
        setPreview(originalImage.path);
      } else if (originalImage.file) {
        getBase64(originalImage.file)
          .then(base64 => {
            setPreview(base64);
          })
      }
    }
  }, [originalImage]);

  const [error, setError] = useState({});

  const onDrop = useCallback(acceptedFiles => {
    if (acceptedFiles[0].size >= allowedMaximumFileSize) {
      return setError({type: 'fileSize', message: `Слишком большой размер файла! Допускаются файлы размером не более ${allowedMaximumFileSize / 1024 / 1024} мБайт.`});
    } else if (allowedMimeTypes.includes(acceptedFiles[0].type)) {
      onChangeOriginalImage(acceptedFiles[0]);
      setError({});
    } else {
      return setError({type: 'mimeTypes', message: 'Недопустимый формат файла! Принимаются файлы типа "image/jpeg".'});
    }
  }, [onChangeOriginalImage]);

  const {
    getRootProps,
    getInputProps,
    isDragActive
  } = useDropzone({onDrop});

  const handleClickDeleteButton = () => {
    onDeleteOriginalImage();
  };

  const renderLoadedImage = () => {
    return <div className='loaded-original-image'>
      <IconButton aria-label="delete" onClick={handleClickDeleteButton}>
        <DeleteIcon/>
      </IconButton>
      <img src={preview} alt={'/'}/>
    </div>
  };

  const renderStartMessage = () => {
    if (error.type === 'mimeTypes') {
      return <p className='message'><span>{error.message}</span></p>
    } else if (error.type === 'fileSize') {
      return <p className='message'><span>{error.message}</span></p>
    } else {
      return <p className='message'><span>Перетащите сюда файл или кликните</span><AddAPhotoIcon/></p>
    }
  };

  const renderDropArea = () => {
    return <>
      <input {...getInputProps()} />
      {isDragActive
        ? <p className='message'><span>Отпустите файл...</span></p>
        : renderStartMessage()
      }
    </>
  };

  const isShowLoadedImage = originalImage && (originalImage.file || originalImage.path);

  return (
    <StyledDropZone {...getRootProps({
      isDragActive: isDragActive,
      originImage: originalImage
    })}>
      {isShowLoadedImage
        ? renderLoadedImage()
        : renderDropArea()}
    </StyledDropZone>
  )
};