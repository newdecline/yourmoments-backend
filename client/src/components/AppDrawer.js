import React from 'react';
import {observer} from "mobx-react";
import styled from "styled-components";
import {NavLink} from "react-router-dom";
import clsx from "clsx";

import {
  Drawer,
  ListItemIcon,
  ListItem,
  List,
} from '@material-ui/core';

import blueGrey from '@material-ui/core/colors/blueGrey';

import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  drawer: {
    width: theme.drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: theme.drawerWidth,
  },
  list: {
    marginTop: 106
  },
  link: {
    display: 'block',
    textDecoration: 'none',
    color: blueGrey[900]
  },
  linkActive: {
    backgroundColor: blueGrey[100]
  },
  linkNested: {
    paddingLeft: 10
  }
}));

export const AppDrawer = observer(props => {
  const {items} = props;

  const classes = useStyles();

  return <StyledAppDrawer
    className={classes.drawer}
    variant="persistent"
    anchor="left"
    open={true}
    classes={{
      paper: classes.drawerPaper,
    }}
  >
    <List className={classes.list}>
      {items.map((item, index) => {
        if (!item.hidden) {
          return (
            <NavLink
              key={index}
              exact
              to={item.as}
              className={item.nested
                ? clsx(classes.link, classes.linkNested)
                : classes.link}
              activeClassName={classes.linkActive}>
              <ListItem button>
                {<ListItemIcon>{item.icon}</ListItemIcon>}
                {item.label}
              </ListItem>
            </NavLink>
          )
        } else {
          return null
        }
      })}
    </List>
  </StyledAppDrawer>
});

const StyledAppDrawer = styled(Drawer)`

`;