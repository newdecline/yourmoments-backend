import React from 'react';
import {authStore} from "../mobx/authStore";

import {NavLink} from "react-router-dom";

import {AppBar, Toolbar, Typography, Button} from "@material-ui/core";

import blueGrey from '@material-ui/core/colors/blueGrey';

import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: blueGrey[700],
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  toolBar: {
    justifyContent: 'space-between'
  },
  link: {
    color: '#fff',
    textDecoration: 'none'
  },
  logoutBtn: {
    color: '#fff',
  }
}));

export const Header = () => {
  const classes = useStyles();

  const handleClickLogoutBtn = () => {
    authStore.logout();
  };

  return <AppBar
    position="fixed"
    className={classes.appBar}
  >
    <Toolbar className={classes.toolBar}>
      <Typography variant="h6" noWrap>
        <NavLink
          to='/'
          className={classes.link}>CMS</NavLink>
      </Typography>
      <Button
        onClick={handleClickLogoutBtn}
        className={classes.logoutBtn}>Выйти</Button>
    </Toolbar>
  </AppBar>
};