import React, {useState} from "react";
import {Controller, useForm} from 'react-hook-form';
import apiService from "../../../utilities/apiService";
import {customHistory} from "../../../customHistory";

import {blueGrey} from '@material-ui/core/colors';

import {makeStyles} from '@material-ui/core/styles';

import {
  Button,
  FormControl,
  MenuItem,
  Select,
  TextField,
  CircularProgress
} from "@material-ui/core";

import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  formControl: {
    margin: '8px 0',
    minWidth: 120,
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
}));

export const CreatePhotoSeriesForm = () => {
  const classes = useStyles();

  const [loading, setLoading] = useState(false);

  const {register, handleSubmit, control, errors} = useForm();

  const onSubmit = async dataValue => {
    setLoading(true);
    const {status, body} = await apiService.createPhotoSeries(dataValue);

    if (status === 201) {
      customHistory.push(`/content/photo-series-update?slug=${body.slug}`);
    }
  };

  const getErrors = () => {
    return {
      name: errors.hasOwnProperty('name')
    };
  };

  if (loading) {
    return <CircularProgress style={{color: blueGrey[900]}}/>
  }

  return <div className={classes.root}>
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormControl>
        <Controller
          as={
            <Select>
              <MenuItem value="paired">Парные</MenuItem>
              <MenuItem value="wedding">Свадебные</MenuItem>
              <MenuItem value="business-content">Контент для бизнеса</MenuItem>
            </Select>
          }
          name="category"
          rules={register({required: true})}
          control={control}
          defaultValue="paired"
        />
      </FormControl>

      <TextField
        error={getErrors().name}
        id="standard-full-width"
        label="Имя серии"
        style={{margin: '16px 0'}}
        placeholder="Введите имя серии"
        helperText="Это имя будет использоваться в качестве имени для серии"
        fullWidth
        margin="normal"
        inputRef={register({required: true})}
        name='name'
        InputLabelProps={{
          shrink: true,
        }}/>

      <Button
        disabled={getErrors().name}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<AddIcon/>}
      >Создать серию</Button>
    </form>
  </div>
};