import React, {useState} from "react";
import {maxBy} from "lodash";
import {useForm} from 'react-hook-form';
import apiService from "../../../utilities/apiService";
import {customHistory} from "../../../customHistory";

import {blueGrey} from '@material-ui/core/colors';

import {makeStyles} from '@material-ui/core/styles';

import {
  Button,
  TextField,
  CircularProgress
} from "@material-ui/core";

import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  formControl: {
    margin: '8px 0',
    minWidth: 120,
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
}));

export const CreateTariffForm = () => {
  const classes = useStyles();

  const [loading, setLoading] = useState(false);

  const {register, handleSubmit, errors} = useForm();

  const onSubmit = async values => {
    setLoading(true);

    try {
      const lastNumber = await getLastNumber();

      const {status, body} = await apiService.createTariff(
        {...values, position: lastNumber + 1}
        );

      if (status === 201) {
        customHistory.push(`/content/tariff-page/${body._id}`);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const getErrors = () => {
    return {
      name: errors.hasOwnProperty('name')
    };
  };

  if (loading) {
    return <CircularProgress style={{color: blueGrey[900]}}/>
  }

  return <div className={classes.root}>
    <form onSubmit={handleSubmit(onSubmit)}>
      <TextField
        error={getErrors().name}
        id="standard-full-width"
        label="Имя тарифа"
        style={{margin: '16px 0'}}
        placeholder="Введите название тарифа"
        helperText="Это имя будет использоваться в качестве имени для тарифа"
        fullWidth
        margin="normal"
        inputRef={register({required: true})}
        name='name'
        InputLabelProps={{
          shrink: true,
        }}/>

      <Button
        disabled={getErrors().name}
        type='submit'
        variant="contained"
        color="primary"
        size="medium"
        className={classes.button}
        startIcon={<AddIcon/>}
      >Создать тариф</Button>
    </form>
  </div>
};

const getLastNumber = async () => {
  const {status, body} = await apiService.getTariffs();
  if (status === 200) {
    if (body.length === 0) {return 0}
    return maxBy(body, 'position').position
  }
};