import React, {useEffect, useState} from "react";
import {observer} from 'mobx-react';
import {useForm} from 'react-hook-form';
import {EditorState, convertToRaw} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import priceStore from "../../../mobx/PriceStore";
import {useNormalizeHTMLtoEditor} from "../../../hooks/useNormalizeHTMLtoEditor";

import {blueGrey} from '@material-ui/core/colors';
import {makeStyles} from '@material-ui/core/styles';

import {
  Button,
  TextField,
  CircularProgress,
  Typography,
  Divider
} from "@material-ui/core";

import {
  Save
} from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    '& label.Mui-focused': {
      color: blueGrey[500],
    },
    '& p.MuiFormHelperText-root.Mui-focused': {
      color: blueGrey[500],
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: blueGrey[500],
    },
  },
  button: {
    marginTop: theme.spacing(2),
    backgroundColor: blueGrey[500],
    color: '#fff',
    '&:hover': {
      backgroundColor: blueGrey[600],
    },
    '&:disabled': {
      backgroundColor: blueGrey[100],
    },
  },
  divider: {
    margin: '30px 0'
  },
  editor: {
    border: '1px solid #f1f1f1'
  },
  editorWrapper: {
    width: '50%'
  }
}));

export const UpdateTariffForm = observer(props => {
  const {id} = props;
  const classes = useStyles();
  const tariff = priceStore.tariff;
  const loading = priceStore.loadingTariff;

  const normalizedResultsHTML1 = useNormalizeHTMLtoEditor(tariff.resultsHTML1);
  const normalizedResultsHTML2 = useNormalizeHTMLtoEditor(tariff.resultsHTML2);
  const normalizedNotesHTML = useNormalizeHTMLtoEditor(tariff.notesHTML);
  const normalizedSurchargeHTML = useNormalizeHTMLtoEditor(tariff.surchargeHTML);

  const initialFormData = {
    name: tariff.name || '',
    duration: tariff.duration || '',
    serviceName1: tariff.serviceName1 || '',
    cost1: tariff.cost1 || '',
    serviceName2: tariff.serviceName2 || '',
    cost2: tariff.cost2 || '',
  };

  const [resultsHTML1, setResultsHTML1] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!tariff.resultsHTML1) {
      return
    }

    setResultsHTML1(normalizedResultsHTML1)
  }, [tariff.resultsHTML1]);

  const [resultsHTML2, setResultsHTML2] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!tariff.resultsHTML2) {
      return
    }

    setResultsHTML2(normalizedResultsHTML2)
  }, [tariff.resultsHTML2]);

  const [notesHTML, setNotesHTML] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!tariff.notesHTML) {
      return
    }

    setNotesHTML(normalizedNotesHTML)
  }, [tariff.notesHTML]);

  const [surchargeHTML, setSurchargeHTML] = useState(EditorState.createEmpty());
  useEffect(() => {
    if (!tariff.surchargeHTML) {
      return
    }

    setSurchargeHTML(normalizedSurchargeHTML)
  }, [tariff.surchargeHTML]);

  const {register, handleSubmit, setValue, errors} = useForm();

  const onSubmit = async values => {
    priceStore.updateTariff({...values, _id: id});
  };

  const getResultsHTMLData = data => {
    switch (data.name) {
      case 'resultsHTML1':
        return data;
      case 'resultsHTML2':
        return data;
      case 'notesHTML':
        return data;
      case 'surchargeHTML':
        return data;
      default:
        return {}
    }
  };

  const getErrors = () => {
    return {
      name: errors.hasOwnProperty('name'),
    };
  };

  const isDisabledButtonSave = () => {
    return getErrors().name
  };

  const onEditorStateChange = (editorState, data) => {
    const {name, setState} = getResultsHTMLData(data);

    setState(editorState);

    if (!editorState.getCurrentContent().hasText()) {
      setValue(name, '');
    } else {
      setValue(name, draftToHtml(convertToRaw(editorState.getCurrentContent())));
    }
  };

  useEffect(() => {
    priceStore.getTariff({_id: id});
    register({name: 'resultsHTML1'});
    register({name: 'resultsHTML2'});
    register({name: 'notesHTML'});
    register({name: 'surchargeHTML'});
  }, []);

  return <div className={classes.root}>
    {loading
      ? <CircularProgress className={classes.progress}/>
      : <>
        <Typography variant="h5" component="h5" gutterBottom>Тариф: {tariff.name}</Typography>

        <form onSubmit={handleSubmit(onSubmit)}>
          <TextField
            error={getErrors().name}
            id="standard-full-width"
            label="Имя тарифа"
            style={{margin: '16px 0'}}
            placeholder="Введите название тарифа"
            helperText="Это имя будет использоваться в качестве имени для тарифа"
            fullWidth
            margin="normal"
            inputRef={register({required: true})}
            name='name'
            defaultValue={initialFormData.name}
            InputLabelProps={{
              shrink: true,
            }}/>
          <TextField
            error={getErrors().duration}
            id="standard-full-width"
            label="Длительность съемки"
            style={{margin: '16px 0'}}
            placeholder="Введите длительность съемки"
            helperText="Это поле будет использоваться в качестве длительность съемки"
            fullWidth
            margin="normal"
            inputRef={register()}
            name='duration'
            defaultValue={initialFormData.duration}
            InputLabelProps={{
              shrink: true,
            }}/>
          <Typography
            style={{fontWeight: 100}}
            variant="body1"
            component="p"
            gutterBottom>Примечание к тарифу</Typography>
          <Editor
            editorState={notesHTML}
            editorClassName={classes.editor}
            wrapperClassName={classes.editorWrapper}
            onEditorStateChange={(editorState) => {
              onEditorStateChange(editorState, {
                name: 'notesHTML',
                state: notesHTML,
                setState: setNotesHTML
              })
            }}
          />

          <Divider className={classes.divider}/>
          <TextField
            error={getErrors().serviceName1}
            id="standard-full-width"
            label="Имя услуги"
            style={{margin: '16px 0'}}
            placeholder="Введите имя услуги"
            helperText="Это поле будет использоваться в качестве имени услуги"
            fullWidth
            margin="normal"
            inputRef={register()}
            name='serviceName1'
            defaultValue={initialFormData.serviceName1}
            InputLabelProps={{
              shrink: true,
            }}/>
          <TextField
            error={getErrors().cost1}
            id="standard-full-width"
            label="Стоимость услуги"
            style={{margin: '16px 0'}}
            placeholder="Введите стоимость услуги"
            helperText="Это поле будет использоваться в качестве стоимости услуги"
            fullWidth
            margin="normal"
            inputRef={register()}
            name='cost1'
            defaultValue={initialFormData.cost1}
            InputLabelProps={{
              shrink: true,
            }}/>
          <Typography
            style={{fontWeight: 100}}
            variant="body1"
            component="p"
            gutterBottom>Что входит в тариф(фотосъемка)</Typography>
          <Editor
            editorState={resultsHTML1}
            editorClassName={classes.editor}
            wrapperClassName={classes.editorWrapper}
            onEditorStateChange={(editorState) => {
              onEditorStateChange(editorState, {
                name: 'resultsHTML1',
                state: resultsHTML1,
                setState: setResultsHTML1
              })
            }}
          />

          <Divider className={classes.divider}/>
          <TextField
            error={getErrors().serviceName2}
            id="standard-full-width"
            label="Имя услуги"
            style={{margin: '16px 0'}}
            placeholder="Введите имя услуги"
            helperText="Это поле будет использоваться в качестве имени услуги"
            fullWidth
            margin="normal"
            inputRef={register()}
            name='serviceName2'
            defaultValue={initialFormData.serviceName2}
            InputLabelProps={{
              shrink: true,
            }}/>
          <TextField
            error={getErrors().cost2}
            id="standard-full-width"
            label="Стоимость услуги"
            style={{margin: '16px 0'}}
            placeholder="Введите стоимость услуги"
            helperText="Это поле будет использоваться в качестве стоимости услуги"
            fullWidth
            margin="normal"
            inputRef={register()}
            name='cost2'
            defaultValue={initialFormData.cost2}
            InputLabelProps={{
              shrink: true,
            }}/>
          <Typography
            style={{fontWeight: 100}}
            variant="body1"
            component="p"
            gutterBottom>Что входит в тариф(видеосъемка)</Typography>
          <Editor
            editorState={resultsHTML2}
            editorClassName={classes.editor}
            wrapperClassName={classes.editorWrapper}
            onEditorStateChange={(editorState) => {
              onEditorStateChange(editorState, {
                name: 'resultsHTML2',
                state: resultsHTML2,
                setState: setResultsHTML2
              })
            }}
          />

          <Divider className={classes.divider}/>
          <Typography
            style={{fontWeight: 100}}
            variant="body1"
            component="p"
            gutterBottom>Возможные доплаты</Typography>
          <Editor
            editorState={surchargeHTML}
            editorClassName={classes.editor}
            wrapperClassName={classes.editorWrapper}
            onEditorStateChange={(editorState) => {
              onEditorStateChange(editorState, {
                name: 'surchargeHTML',
                state: surchargeHTML,
                setState: setSurchargeHTML
              })
            }}
          />

          <Button
            disabled={isDisabledButtonSave()}
            type='submit'
            variant="contained"
            color="primary"
            size="medium"
            className={classes.button}
            startIcon={<Save/>}
          >Сохранить</Button>
        </form>
      </>
    }
  </div>
});