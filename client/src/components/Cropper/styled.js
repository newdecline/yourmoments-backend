import styled from 'styled-components';

export const StyledCropper = styled('div')`
  display: flex;
  flex-direction: row-reverse;
  justify-content: flex-end;
  width: 100%;
  max-width: 100%;
  .cropped-image-container {
    position: relative;
    display: flex;
    flex: 1 1 auto;
    min-height: 400px;
    background-color: #000;
  }
  .cropped-image {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
`;