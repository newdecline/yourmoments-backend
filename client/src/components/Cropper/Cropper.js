import React, {forwardRef, useEffect, useState} from 'react';
import CropperModule from 'react-cropper';
import {StyledCropper} from "./styled";
import {getBase64} from "../../utilities/getBase64";

export const Cropper = forwardRef((props, ref) => {
  const {
    croppedImage,
    originalImage,
    aspectRatio,
  } = props;

  const [initOriginalImage, setInitOriginalImage] = useState(undefined);

  useEffect(() => {
    if (originalImage) {
      if (originalImage.hasOwnProperty('path')) {
        setInitOriginalImage(originalImage.path);
      } else if (originalImage.file) {
        getBase64(originalImage.file)
          .then(base64 => {
            setInitOriginalImage(base64);
          })
      }
    }
  }, [originalImage]);

  const initialCroppedImage = () => {
    if (croppedImage.hasOwnProperty('path')) {
      return croppedImage.path;
    } else if (croppedImage.hasOwnProperty('file')) {
      return URL.createObjectURL(croppedImage.file);
    }
  };

  const isShowCroppedImage = croppedImage && (croppedImage.file || croppedImage.path);

  return <StyledCropper>
    <div className="cropped-image-container">
      {isShowCroppedImage && <img
        className='cropped-image'
        src={initialCroppedImage()}
        alt="cropped-image"/>}
    </div>

    <CropperModule
      style={{height: 400, width: 640}}
      aspectRatio={aspectRatio}
      viewMode={1}
      guides={false}
      src={initOriginalImage}
      ref={ref}
    />
  </StyledCropper>
});