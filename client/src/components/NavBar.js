import React from 'react';
import styled from "styled-components";
import {NavLink} from 'react-router-dom';

import {
  AppBar,
  Toolbar
} from "@material-ui/core";

import {makeStyles} from "@material-ui/core/styles";

import blueGrey from "@material-ui/core/colors/blueGrey";


const useStyles = makeStyles(theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    top: 64,
    backgroundColor: blueGrey[200],
  },
  toolbar: {
    minHeight: 'auto',
    justifyContent: 'center'
  },
  link: {
    padding: '10px 10px 7px 10px',
    margin: '0 5px',
    color: blueGrey[900],
    textDecoration: 'none',
    borderBottom: `3px solid transparent`,
    boxSizing: 'border-box'
  },
  linkActive: {
    borderBottom: `3px solid ${blueGrey[100]}`,
  }
}));

const navLinks = [
  {
    label: 'Контент',
    value: '/content',
  },
  // {
  //     label: 'SEO',
  //     value: '/seo',
  // },
];

export const NavBar = () => {
  const classes = useStyles();

  return  <StyledNavBar
      position="fixed"
      className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        {navLinks.map(link => {
          return (
            <NavLink
              key={link.value}
              to={link.value}
              className={classes.link}
              activeClassName={classes.linkActive}>{link.label}
            </NavLink>
          )
        })}
      </Toolbar>
  </StyledNavBar>
};

const StyledNavBar = styled(AppBar)`

`;