require('dotenv').config();
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser  = require("body-parser");
const routesErrorHandler = require('./routes/routesErrorHandler');
const app = express();
const isProduction = process.env.NODE_ENV === 'production';

const mongooseOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
};

mongoose
  .connect(process.env.MONGODB_URI, mongooseOptions)
  .then(() => {
    if (!isProduction) {
      mongoose.set('debug', true);
    }

    app.use(bodyParser.urlencoded({ extended: false }));
    app.use('/uploads', express.static('uploads'));

    app.use(require('./routes'));

    app.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, 'client/build/index.html'));
    });

    routesErrorHandler(app);

    app.listen(process.env.PORT || 3000, () => {
      console.log(`Сервер запущен на порту ${process.env.PORT}`);
    });
  })
  .catch(err => {
    console.log(err);
  });
